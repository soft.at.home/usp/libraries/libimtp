# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v2.2.1 - 2024-10-10(11:00:15 +0000)

### Other

- USP broker socket should be accessible to non-root users

## Release v2.2.1 - 2024-10-10(10:58:47 +0000)

### Other

- USP broker socket should be accessible to non-root users

## Release v2.2.0 - 2024-06-18(08:24:29 +0000)

### New

- Add new function to get connection flags

## Release v2.1.2 - 2024-06-14(11:32:26 +0000)

### Other

- [CI] add wnc-lvr5 compiler

## Release v2.1.1 - 2024-04-30(13:30:21 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v2.1.0 - 2024-01-30(15:39:56 +0000)

### New

- [libimtp] Add file descriptor transfer functionality

## Release v2.0.2 - 2024-01-04(08:51:40 +0000)

### Other

- Update documentation
- Move library to prpl gitlab

## Release v2.0.1 - 2023-09-06(07:17:09 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v2.0.0 - 2023-07-10(11:26:21 +0000)

### Breaking

- [IMTP] Implement IMTP communication as specified in TR-369

## Release v1.6.1 - 2023-05-03(10:11:32 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v1.6.0 - 2023-01-12(10:47:42 +0000)

### Other

- [SAHPairing] Make sahpairing work with MQTT client

## Release v1.5.2 - 2023-01-11(14:18:39 +0000)

### Other

- Update dependencies in .gitlab-ci.yml

## Release v1.5.1 - 2022-11-10(11:41:35 +0000)

### Other

- [Packet Interception] Create the new Packet Interception component

## Release v1.5.0 - 2022-10-20(08:32:54 +0000)

### New

- Need separate type for JSON encoded MQTT properties

## Release v1.4.2 - 2022-08-31(13:13:26 +0000)

### Fixes

- Blocking read should not spam logs

## Release v1.4.1 - 2022-05-19(12:33:48 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v1.4.0 - 2022-02-03(14:33:09 +0000)

### New

- [IMTP] TLV of type endpoint_id is needed

## Release v1.3.1 - 2022-01-28(11:00:59 +0000)

### Fixes

- Need to reread large IMTP messages

### Other

- Solve open source issues
- [USPAgent] The USP Agent must be opensourced to gitlab.com/soft.at.home

## Release v1.3.0 - 2021-10-18(13:57:11 +0000)

### New

- Add documentation for TLV messages

## Release v1.2.0 - 2021-07-19(13:19:58 +0000)

### New

- [USP][CDROUTER] USP MQTT Publish messages includes invalid Content Type property (empty)

## Release v1.1.10 - 2021-07-12(12:09:04 +0000)

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Other

- Migrate to new licenses format (baf)

## Release 1.1.9 - 2021-02-16(07:31:41 +0000)

### Added

- Add subscribe tlv type

## Release 1.1.8 - 2021-02-01(16:12:40 +0000)

### Changed

- Remove accepted stream connection from connections list when connection is closed
- Update example makefiles

## Release 1.1.7 - 2020-12-08(10:59:59 +0000)

### Added

- Add -I$(STAGINGDIR)/usr/include to CFLAGS and -L$(STAGINGDIR)/usr/lib to LDFLAGS
- Add baf files

## Release 1.1.6 - 2020-11-19(10:02:16 +0100)

### Changed

- Use sofa pipelines

## Release 1.1.5 - 2020-11-10(10:09:16 +0000)

### Added

- Add build instructions and technical user guide to README

### Fixed

- Issue: icon->addr_other must be allocated when using datagrams

## Release 1.1.4 - 2020-11-04(08:45:23 +0000)

### Changed

- Don't unlink socket address before binding

## Release 1.1.3 - 2020-10-23(10:21:17 +0000)

### Changed

- Fix typo in linker path

## Release 1.1.2 - 2020-10-23(09:52:05 +0000)

### Changed

- Fix symlinks and package name for debian package

## Release 1.1.0 - 2020-10-22(16:19:10 +0000)

### Added

- Add doxygen generation and deploy job.
- Add LICENSE and CHANGELOG.md.
- Add debian package creation files.

### Changed

- Small updates to documentation.

## Release 1.0.0 - 2020-10-22(10:11:10 +0000)

### Added

- Implement construction and (de)-serialization of TLV messages using a more general API.
- Send TLVs over unix domain sockets using datagrams.
- Send TLVs over unix domain sockets using streams.
- Provide basic communication examples for using the library.
- Update the unit tests.

