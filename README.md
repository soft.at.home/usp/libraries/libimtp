# Generic implementation for the internal message protocol 

[[_TOC_]]

## Introduction

libimtp is a library which provides functionality to set up a connection between two internal USP endpoints using a unix domain socket. It provides a very basic protocol based on TLVs. (Type, Length, Value pairs)

### Why libimtp?

USP is designed to work with protobufs. The current USP requirements only specify protocols based on IP to transport protobuf between USP endpoints.
This is highly inefficient if both USP endpoints reside on the same physical host.
The proposal today is to use  unix domain sockets as a communication mechanism and define a very lightweight TLV based system to encapsulate the protobufs.
Using TLVs gives us the flexibility to add a number of extra meta data to a message.
This allows us to send IMTP messages from a USP agent to an MQTT client that runs in a separate process and vice versa.

## Build instructions

Before building this library, its dependencies need to be installed:

```
sudo apt update && sudo apt install libamxc liburiparser-dev sah-lib-sahtrace-dev
```

Then the library can be built with

```
make && sudo -E make install
```

## IMTP technical user guide

`libimtp` was initially written at the end of 2020 before anything regarding the IMTP was
standardized. In the initial version, we decided to use a TLV of type(T) imtp_tlv_type_head as a
header, where the length(L) indicated the length of the following TLVs. This header TLV did not
have a value(V), but was instead immediately followed by the type of the next TLV.

After standardizing the IMTP, we ended up with an IMTP Frame format that consists out of a header
with 4 synchronization bytes 0x5f, 0x55, 0x53, 0x50, which spell "_USP", followed by the length of
the remaining TLVs.

Currently this library supports APIs for building, sending and receiving the original TLV only IMTP
messages as well as the newly standardized IMTP frames. It also supports the sending of file descriptors over an IMTP connection.

> WARNING  
> We suggest that anyone who uses this library uses the new APIs for the standardized IMTP frames.
> The old TLV only APIs may be deprecated in the future.

### IMTP Frame layout

As mentioned in the previous section, an IMTP Frame consists out of a header followed by one or
several TLVs. An example of an IMTP Frame with a Handshake TLV is shown below:

![](doc/images/frame_example_1.png) 

The IMTP Frame always starts with an optional synchronization part. In case IMTP Frames are used to
communicate with a USP Broker or Service, the synchronization part will be 0x5f, 0x55, 0x53, 0x50
as shown in the example above. However since we are using reliable unix domain sockets for
communication, the synchronization bytes aren't really needed and can be omitted when USP is not
used (or different sync bytes can be used). After the sync bytes, the full length of the TLVs that
follow will be encoded in network byte order. Then the actual TLVs will follow.

There are currently 3 TLV types standardized:

- imtp_tlv_type_handshake = 1: payload is a UTF-8 string that represents the Endpoint ID of the USP Endpoint
- imtp_tlv_type_error = 2: payload is a UTF-8 string that provides the error message
- imtp_tlv_type_protobuf_bytes = 3: payload is binary protobuf

> Note:  
> Officially the 3rd TLV type contains a USP protobuf message, but we decided to keep it as a more generic binary protobuf

We define several other custom TLV types as well. Some examples are:

- imtp_tlv_type_topic: payload is a UTF-8 string that represents an MQTT topic. This is for example used for sending a USP protobuf together with an MQTT topic to our standalone tr181-mqtt client such that it knows on which topic it needs to publish the message.
- imtp_tlv_type_subscribe: payload is a UTF-8 string that represents a data model path. This is used by our USP back-end to implement the amxb_subscribe function to subscribe to an object path.
- imtp_tlv_type_json: payload is a JSON string. This can be used to transport any JSON encoded data over the IMTP.

Always make sure that both interlocutors know the TLV types that are used to avoid communication problems.

### IMTP TLV message layout (legacy)

This section describes the original TLV message layout that should no longer be used.

An IMTP TLV message consists of multiple TLVs where the first TLV is always of Type head. The Length
of this header TLV indicates the full length of the following TLV messages. The header TLV doesn't
have a Value, but is immediately followed by the next TLV. The same TLV types as for an IMTP Frame
can be used. For a full list, refer to the `imtp_message.h` header file.

In this library, the TLV Type is encoded with 1 byte and Length is encoded in 4 bytes. If we want to
send an IMTP message that consists of 2 protobuf TLVs with a length of 1024 bytes, the message would
look like this (with the length in network byte order):

![](doc/images/tlv_example_3.png) 

To clarify, the head TLV has type 0 (0x00) and length 2058 (0x0000080a), which is
2 * (1024 + 5) bytes. The 5 extra bytes are needed for the type and length of the protobuf TLVs.
Both protobuf TLVs are in this example identical and have a type of 3 (0x00000001) and a length of
1024 (0x00000400) bytes.

Next is an example IMTP message with a TLV of type Topic (type = 200 = 0xc8). The value of the topic
is "mqtt-topic".

![](doc/images/tlv_example_4.png) 

### Creating TLV messages

### Setting up IMTP connections

A unix domain socket address is represented by the following structure:

```
struct sockaddr_un {
    sa_family_t sun_family;               /* AF_UNIX */
    char        sun_path[108];            /* pathname */
};
```

An `imtp_connection_t` struct looks like this:

```
typedef struct _imtp_connection {
    int fd;
    struct sockaddr_un* addr_self;
    struct sockaddr_un* addr_other;
    bool (* imtp_connection_accept_cb)(struct _imtp_connection*);
    uint32_t flags;
    amxc_llist_it_t it;
    uint8_t* buffer;
    ssize_t read_len;
} imtp_connection_t;
```

Its values are initialized by calling one of the following functions:

- `int imtp_connection_connect(imtp_connection_t** icon, const char* from_uri, const char* to_uri);`
- `int imtp_connection_listen(imtp_connection_t** icon, const char* uri, imtp_connection_accept_cb_t fn);`

The struct should be deleted with:

- `void imtp_connection_delete(imtp_connection_t** icon);`

The file descriptor `fd` is used to read/write data or to accept incoming connections when using
stream sockets. The `addr_self` and `addr_other` structs are used to identify the socket on the
filesystem by setting the `sun_path` parameter to the right uri value. The callback
`imtp_connection_accept_cb` callback function matches the function passed to
`imtp_connection_listen`. It can be used to filter out incoming connections in case you only want
to accept connections under some given conditions. The flags parameter is used to determine whether
we are dealing with a datagram or stream socket and whether it is a stream listen socket. 
The buffer and read_len parameters are used to read partial IMTP messages. It is possible that you
are reading a very large IMTP message that can't be ready entirely from the first read on the fd.
In this case, the part that is already read is stored in `buffer` and the number of bytes read is
stored in `read_len`. When the rest of the message is available, the fd can be read again to get
the rest of the message.
None of these struct values need to be accessed directly when using this library. Only the file
descriptor is of interest outside the library and can be obtained with `imtp_connection_get_fd`.

#### Stream sockets

In case we are dealing with **stream sockets**, we will use `imtp_connection_connect` for the
client side and `imtp_connection_listen` at the server side.

At the server side we call:

```
imtp_connection_t* icon = NULL;
char* uri = "imtp:/tmp/server";
imtp_connection_listen(&icon, uri, NULL);
```

This opens up a listen socket at the server side that clients can connect to. The value of
`icon->addr_self->sun_path` is set to the passed `uri`. Other processes that want to communicate
with the server should use this to connect to. The value of `icon->addr_other` is not used at the
server side in case of stream connections. The `icon->fd` should be added to an event loop to listen
for incoming connections. Once data is available on the file descriptor `imtp_connection_accept`
can be called to accept the new connection. This returns a file descriptor to the new connection
in case of success. Afterwards `imtp_connection_get_con` can be used to retrieve the connection
that corresponds to the new file descriptor. This connection is used during
`imtp_connection_write_frame` or `imtp_connection_read_frame` to actually send and receive data.

> Note:  
> When using pure TLV messages instead of IMTP Frames, the functions `imtp_connection_write` and
> `imtp_connection_read` can be used.

On the client side we call:

```
imtp_connection_t* icon = NULL;
char* to_uri = "imtp:/tmp/server";
imtp_connection_connect(&icon, NULL, to_uri);
```

In this situation `icon->addr_self` is not used, but `icon->addr_other->sun_path` is set to the
passed `uri` and a connection is made to this uri. If it is accepted, the `icon` struct can be used
to read and write data to the correspondent.

#### Datagram sockets

In case we are dealing with **datagram sockets**, we will use `imtp_connection_connect` for both
the client side and server side.

On the server side we call

```
imtp_connection_t* icon = NULL;
char* from_uri = "imtp:/tmp/server";
imtp_connection_connect(&icon, from_uri, NULL);
```

`to_uri` can be `NULL`, because we don't know yet which client will be connecting. In this
situation, there is no actual connection happening, but a socket is created for the client to
connect to. The value of `icon->addr_self->sun_path` is set to `from_uri`.

On the client side we call

```
imtp_connection_t* icon = NULL;
char* from_uri = "imtp:/tmp/client";
char* to_uri = "imtp:/tmp/server";
imtp_connection_connect(&icon, from_uri, to_uri);
```

Here we do make an actual connection to the socket of the server (notice the matching uris). Again
`icon->addr_self->sun_path = from_uri` and now `icon->addr_other->sun_path = to_uri`.

When data is sent from the client to the server, the server's `icon->to_uri->sun_path` will be set
to the value of the client's `icon->from_uri->sun_path` such that it knows how to reply. This means
that the client must always send something first to the server before it is able to reply.

#### File descriptors

It is also possible to send a file descriptor over datagram or stream sockets.
The file descriptor can be sent by using the function `imtp_connection_write_fd`.
To receive a file descriptor, the other side should use the `imtp_connection_read_fd` function.
To signal that a file descriptor will be sent, the sending side can first send a TLV with type `imtp_type_tlv_fd`. 
The receiving side will then know that the next data should be read by using `imtp_connection_read_fd`.
