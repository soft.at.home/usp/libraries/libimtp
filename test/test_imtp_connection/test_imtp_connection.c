/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <amxc/amxc_macros.h>

#include "imtp/imtp_connection.h"
#include "imtp_dummy.h"
#include "test_imtp_connection.h"
#include "imtp_priv.h"
#include "imtp_connection_priv.h"
#include "socket_mock.h"

static bool dummy_accept_cb(UNUSED imtp_connection_t* icon) {
    return true;
}

void test_imtp_connection_connect(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* from_uri = "/tmp/test-imtp-from";
    const char* to_uri = "/tmp/test-imtp-to";

    expect_any_count(__wrap_connect, sockfd, 2);
    expect_any_count(__wrap_connect, addr, 2);
    expect_any_count(__wrap_connect, addrlen, 2);
    will_return_always(__wrap_connect, 0);

    // Test creation of datagram client connection
    assert_int_equal(imtp_connection_connect(&icon, from_uri, to_uri), 0);
    assert_non_null(icon);
    assert_non_null(icon->addr_self);
    assert_non_null(icon->addr_other);
    assert_int_equal(strncmp(icon->addr_self->sun_path, from_uri, strlen(from_uri)), 0);
    assert_int_equal(strncmp(icon->addr_other->sun_path, to_uri, strlen(to_uri)), 0);
    assert_true((imtp_connection_get_flags(icon) & SOCK_DGRAM) == SOCK_DGRAM);
    imtp_connection_delete(&icon);
    assert_null(icon);

    // Test creation of datagram server connection
    assert_int_equal(imtp_connection_connect(&icon, from_uri, NULL), 0);
    assert_non_null(icon);
    assert_non_null(icon->addr_self);
    assert_non_null(icon->addr_other);
    assert_int_equal(strncmp(icon->addr_self->sun_path, from_uri, strlen(from_uri)), 0);
    assert_true((imtp_connection_get_flags(icon) & SOCK_DGRAM) == SOCK_DGRAM);
    imtp_connection_delete(&icon);
    assert_null(icon);

    // Test creation of stream client connection
    assert_int_equal(imtp_connection_connect(&icon, NULL, to_uri), 0);
    assert_non_null(icon);
    assert_non_null(icon->addr_other);
    assert_int_equal(strncmp(icon->addr_other->sun_path, to_uri, strlen(to_uri)), 0);
    assert_true((imtp_connection_get_flags(icon) & SOCK_STREAM) == SOCK_STREAM);
    imtp_connection_delete(&icon);
    assert_null(icon);

    // Test creation of stream server connection (listen socket)
    assert_int_equal(imtp_connection_listen(&icon, from_uri, NULL), 0);
    assert_non_null(icon);
    assert_non_null(icon->addr_self);
    assert_int_equal(strncmp(icon->addr_self->sun_path, from_uri, strlen(from_uri)), 0);
    assert_true((imtp_connection_get_flags(icon) & SOCK_STREAM) == SOCK_STREAM);
    assert_true((imtp_connection_get_flags(icon) & IMTP_LISTEN) == IMTP_LISTEN);
    imtp_connection_delete(&icon);
    assert_null(icon);
    return;
}

void test_imtp_connection_invalid(UNUSED void** state) {
    imtp_connection_t* icon = NULL;

    assert_int_equal(imtp_connection_connect(NULL, NULL, NULL), -1);
    assert_int_equal(imtp_connection_listen(NULL, NULL, NULL), -1);
    assert_int_equal(imtp_connection_listen(&icon, NULL, NULL), -1);
    assert_int_equal(imtp_connection_get_flags(NULL), -1);
    assert_null(icon);

    return;
}

void test_imtp_connection_multiple(UNUSED void** state) {
    imtp_connection_t* icon1 = NULL;
    imtp_connection_t* icon2 = NULL;
    const char* uri = "imtp:/tmp/test.sock";

    assert_int_equal(imtp_connection_listen(&icon1, uri, NULL), 0);
    assert_non_null(icon1);
    assert_int_equal(strcmp(icon1->addr_self->sun_path, "/tmp/test.sock"), 0);
    assert_int_equal(icon1->flags, SOCK_STREAM | IMTP_LISTEN);

    assert_int_equal(imtp_connection_listen(&icon2, uri, NULL), -1);
    assert_null(icon2);

    imtp_connection_delete(&icon1);
    assert_null(icon1);
    return;
}

void test_imtp_connection_delete_invalid(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* from_uri = "/tmp/test-imtp";
    struct sockaddr_un* addr_self = NULL;

    imtp_connection_delete(NULL);
    imtp_connection_delete(&icon);
    assert_null(icon);

    assert_int_equal(imtp_connection_connect(&icon, from_uri, NULL), 0);
    assert_non_null(icon);
    // Try to delete connection without addr_self
    addr_self = icon->addr_self;
    icon->addr_self = NULL;
    imtp_connection_delete(&icon);
    assert_null(icon);

    unlink(addr_self->sun_path);
    free(addr_self);
    return;
}

void test_imtp_connection_accept(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* from_uri = "/tmp/test-imtp";

    assert_int_equal(imtp_connection_listen(&icon, from_uri, NULL), 0);
    expect_any(__wrap_accept, sockfd);
    expect_any(__wrap_accept, addr);
    expect_any(__wrap_accept, addrlen);
    will_return(__wrap_accept, 10);

    assert_int_equal(imtp_connection_accept(icon), 10);

    imtp_connection_delete(&icon);
}

void test_imtp_connection_accept_cb(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* from_uri = "/tmp/test-imtp";

    assert_int_equal(imtp_connection_listen(&icon, from_uri, dummy_accept_cb), 0);
    expect_any(__wrap_accept, sockfd);
    expect_any(__wrap_accept, addr);
    expect_any(__wrap_accept, addrlen);
    will_return(__wrap_accept, 10);

    assert_int_equal(imtp_connection_accept(icon), 10);

    imtp_connection_delete(&icon);
}

void test_imtp_connection_accept_invalid(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* from_uri = "/tmp/test-imtp-from";

    assert_int_equal(imtp_connection_accept(NULL), -1);

    assert_int_equal(imtp_connection_connect(&icon, from_uri, NULL), 0);
    assert_non_null(icon);
    assert_int_equal(strncmp(icon->addr_self->sun_path, from_uri, strlen(from_uri)), 0);
    assert_int_equal(icon->flags, SOCK_DGRAM);
    assert_int_equal(imtp_connection_accept(icon), -1);

    imtp_connection_delete(&icon);
    assert_null(icon);

    assert_int_equal(imtp_connection_listen(&icon, from_uri, NULL), 0);
    expect_any(__wrap_accept, sockfd);
    expect_any(__wrap_accept, addr);
    expect_any(__wrap_accept, addrlen);
    will_return(__wrap_accept, -1);
    assert_int_equal(imtp_connection_accept(icon), -1);

    imtp_connection_delete(&icon);
    assert_null(icon);
}

void test_imtp_connection_write_dgram(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_client = "/tmp/test-imtp-client";
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, uri_client, uri_server), 0);

    create_dummy_tlv(&tlv);
    expect_any(__wrap_sendto, sockfd);
    expect_any(__wrap_sendto, buf);
    expect_any(__wrap_sendto, len);
    expect_any(__wrap_sendto, flags);
    expect_any(__wrap_sendto, dest_addr);
    expect_any(__wrap_sendto, addrlen);
    will_return(__wrap_sendto, 0);
    assert_int_equal(imtp_connection_write(icon, tlv), 0);

    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_write_stream(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    create_dummy_tlv(&tlv);
    expect_any(__wrap_write, fd);
    expect_any(__wrap_write, buf);
    expect_any(__wrap_write, count);
    will_return(__wrap_write, 0);
    assert_int_equal(imtp_connection_write(icon, tlv), 0);

    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_write_fd(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    const char* test_file = "/tmp/test-file";
    int fd_send = -1;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    fd_send = open(test_file, O_CREAT | O_RDONLY);
    assert_int_not_equal(fd_send, -1);
    expect_any(__wrap_sendmsg, fd);
    expect_any(__wrap_sendmsg, message);
    expect_any(__wrap_sendmsg, flags);
    expect_value(__wrap_sendmsg, send_fd, fd_send);
    will_return(__wrap_sendmsg, 0);
    assert_int_equal(imtp_connection_write_fd(icon, fd_send), 0);

    close(fd_send);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_write_invalid(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_client = "/tmp/test-imtp";
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, uri_client, uri_server), 0);

    assert_int_equal(imtp_connection_write(NULL, tlv), -1);
    assert_int_equal(imtp_connection_write(icon, NULL), -1);

    create_dummy_tlv(&tlv);
    expect_any(__wrap_sendto, sockfd);
    expect_any(__wrap_sendto, buf);
    expect_any(__wrap_sendto, len);
    expect_any(__wrap_sendto, flags);
    expect_any(__wrap_sendto, dest_addr);
    expect_any(__wrap_sendto, addrlen);
    will_return(__wrap_sendto, -1);
    assert_int_equal(imtp_connection_write(icon, tlv), -1);

    icon->flags = 0;
    assert_int_equal(imtp_connection_write(icon, tlv), -1);

    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_dgram(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_client = "/tmp/test-imtp-client";
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, uri_client, uri_server), 0);

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, 5);
    will_return(__wrap_recvfrom, 1);
    will_return(__wrap_recvfrom, 5);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, tlv_dummy->length);
    will_return(__wrap_recvfrom, 2);
    will_return(__wrap_recvfrom, tlv_dummy->length);

    assert_int_equal(imtp_connection_read(icon, &tlv), 0);
    assert_non_null(tlv);
    assert_int_equal(tlv->type, tlv_dummy->type);
    assert_int_equal(tlv->length, tlv_dummy->length);
    assert_non_null(tlv->value);
    assert_non_null(tlv->next);

    assert_int_equal(tlv->next->type, tlv_dummy->next->type);
    assert_int_equal(tlv->next->length, tlv_dummy->next->length);
    assert_non_null(tlv->next->value);
    assert_null(tlv->next->next);

    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_dgram_partial(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_client = "/tmp/test-imtp-client";
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;
    int len = 10;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, uri_client, uri_server), 0);

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, 5);
    will_return(__wrap_recvfrom, 1);
    will_return(__wrap_recvfrom, 5);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, tlv_dummy->length);
    will_return(__wrap_recvfrom, 3);                        // case partial
    will_return(__wrap_recvfrom, tlv_dummy->length - len);  // bytes_to_copy
    will_return(__wrap_recvfrom, 0);                        // offset
    will_return(__wrap_recvfrom, tlv_dummy->length - len);  // return value

    assert_int_equal(imtp_connection_read(icon, &tlv), 1);
    assert_null(tlv);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, len);
    will_return(__wrap_recvfrom, 3);                        // case partial
    will_return(__wrap_recvfrom, len);                      // bytes_to_copy
    will_return(__wrap_recvfrom, tlv_dummy->length - len);  // offset
    will_return(__wrap_recvfrom, len);                      // return value
    assert_int_equal(imtp_connection_read(icon, &tlv), 0);

    assert_non_null(tlv);
    assert_int_equal(tlv->type, tlv_dummy->type);
    assert_int_equal(tlv->length, tlv_dummy->length);
    assert_non_null(tlv->value);
    assert_non_null(tlv->next);

    assert_int_equal(tlv->next->type, tlv_dummy->next->type);
    assert_int_equal(tlv->next->length, tlv_dummy->next->length);
    assert_non_null(tlv->next->value);
    assert_null(tlv->next->next);

    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_stream(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, 5);
    will_return(__wrap_read, 1);
    will_return(__wrap_read, 5);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, tlv_dummy->length);
    will_return(__wrap_read, 2);
    will_return(__wrap_read, tlv_dummy->length);

    assert_int_equal(imtp_connection_read(icon, &tlv), 0);
    assert_non_null(tlv);
    assert_int_equal(tlv->type, tlv_dummy->type);
    assert_int_equal(tlv->length, tlv_dummy->length);
    assert_non_null(tlv->value);
    assert_non_null(tlv->next);

    assert_int_equal(tlv->next->type, tlv_dummy->next->type);
    assert_int_equal(tlv->next->length, tlv_dummy->next->length);
    assert_non_null(tlv->next->value);
    assert_null(tlv->next->next);

    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_stream_partial(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;
    int len = 10;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, 5);
    will_return(__wrap_read, 1);
    will_return(__wrap_read, 5);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, tlv_dummy->length);
    will_return(__wrap_read, 3);                            // case partial
    will_return(__wrap_read, tlv_dummy->length - len);      // bytes_to_copy
    will_return(__wrap_read, 0);                            // offset
    will_return(__wrap_read, tlv_dummy->length - len);      // return value
    assert_int_equal(imtp_connection_read(icon, &tlv), 1);
    assert_null(tlv);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, len);
    will_return(__wrap_read, 3);                            // case partial
    will_return(__wrap_read, len);                          // bytes_to_copy
    will_return(__wrap_read, tlv_dummy->length - len);      // offset
    will_return(__wrap_read, len);                          // return value
    assert_int_equal(imtp_connection_read(icon, &tlv), 0);

    assert_non_null(tlv);
    assert_int_equal(tlv->type, tlv_dummy->type);
    assert_int_equal(tlv->length, tlv_dummy->length);
    assert_non_null(tlv->value);
    assert_non_null(tlv->next);

    assert_int_equal(tlv->next->type, tlv_dummy->next->type);
    assert_int_equal(tlv->next->length, tlv_dummy->next->length);
    assert_non_null(tlv->next->value);
    assert_null(tlv->next->next);

    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_fd(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    int recv_fd = -1;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    expect_any(__wrap_recvmsg, fd);
    expect_any(__wrap_recvmsg, message);
    expect_any(__wrap_recvmsg, flags);
    will_return(__wrap_recvmsg, 5);
    will_return(__wrap_recvmsg, 5);

    assert_int_equal(imtp_connection_read_fd(icon, &recv_fd), 0);
    assert_int_equal(recv_fd, 5);

    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_invalid(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_client = "/tmp/test-imtp-client";
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;

    imtp_connection_read(NULL, &tlv);

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, uri_client, uri_server), 0);
    imtp_connection_read(icon, NULL);

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, 5);
    will_return(__wrap_recvfrom, 0);
    will_return(__wrap_recvfrom, -1);

    assert_int_equal(imtp_connection_read(icon, &tlv), -1);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, 5);
    will_return(__wrap_recvfrom, 1);
    will_return(__wrap_recvfrom, 5);

    expect_any(__wrap_recvfrom, sockfd);
    expect_any(__wrap_recvfrom, buf);
    expect_any(__wrap_recvfrom, flags);
    expect_any(__wrap_recvfrom, dest_addr);
    expect_any(__wrap_recvfrom, addrlen);
    expect_value(__wrap_recvfrom, len, tlv_dummy->length);
    will_return(__wrap_recvfrom, 0);
    will_return(__wrap_recvfrom, -1);
    assert_int_equal(imtp_connection_read(icon, &tlv), -1);
    imtp_connection_delete(&icon);

    assert_int_equal(imtp_connection_listen(&icon, uri_server, NULL), 0);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_any(__wrap_read, count);
    will_return(__wrap_read, 1);
    will_return(__wrap_read, 5);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_any(__wrap_read, count);
    will_return(__wrap_read, 0);
    will_return(__wrap_read, -1);
    assert_int_equal(imtp_connection_read(icon, &tlv), -1);

    imtp_connection_delete(&icon);
    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
}

void test_imtp_connection_can_peek(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_client = "/tmp/test-imtp-client";
    const char* uri_server = "/tmp/test-imtp-server";
    uint8_t* buffer = calloc(1, 5);
    uint32_t len = 8;
    imtp_tlv_type_t tlv_type = imtp_tlv_type_max;
    uint32_t imtp_len = 0;
    imtp_tlv_t* tlv_dummy = NULL;

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, uri_client, uri_server), 0);

    assert_int_equal(imtp_connection_peek(NULL, buffer, len), -1);
    assert_int_equal(imtp_connection_peek(icon, NULL, len), -1);

    expect_any(__wrap_recv, sockfd);
    expect_any(__wrap_recv, buf);
    expect_any(__wrap_recv, len);
    expect_any(__wrap_recv, flags);
    will_return(__wrap_recv, 1);
    will_return(__wrap_recv, 5);
    assert_int_equal(imtp_connection_peek(icon, buffer, len), 0);

    memcpy(&tlv_type, buffer, 1);
    assert_true(tlv_type == imtp_tlv_type_head);
    memcpy(&imtp_len, buffer + 1, 4);
    assert_true(imtp_len == ntohl(tlv_dummy->length));

    free(buffer);
    imtp_connection_delete(&icon);
    imtp_tlv_delete(&tlv_dummy);
}

void test_can_delete_after_partial_read(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_dummy = NULL;
    int len = 10;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    create_dummy_tlv(&tlv_dummy);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, 5);
    will_return(__wrap_read, 1);
    will_return(__wrap_read, 5);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, tlv_dummy->length);
    will_return(__wrap_read, 3);                            // case partial
    will_return(__wrap_read, tlv_dummy->length - len);      // bytes_to_copy
    will_return(__wrap_read, 0);                            // offset
    will_return(__wrap_read, tlv_dummy->length - len);      // return value
    assert_int_equal(imtp_connection_read(icon, &tlv), 1);
    assert_null(tlv);

    imtp_tlv_delete(&tlv_dummy);
    imtp_tlv_delete(&tlv);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_write_frame(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_frame_t* frame = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    create_dummy_frame(&frame);

    expect_any(__wrap_write, fd);
    expect_any(__wrap_write, buf);
    expect_any(__wrap_write, count);
    will_return(__wrap_write, 0);
    assert_int_not_equal(imtp_connection_write_frame(NULL, NULL), 0);
    assert_int_not_equal(imtp_connection_write_frame(icon, NULL), 0);
    assert_int_equal(imtp_connection_write_frame(icon, frame), 0);

    imtp_frame_delete(&frame);
    imtp_connection_delete(&icon);
}

void test_imtp_connection_read_frame(UNUSED void** state) {
    imtp_connection_t* icon = NULL;
    const char* uri_server = "/tmp/test-imtp-server";
    imtp_frame_t* frame_send = NULL;
    imtp_frame_t* frame_recv = NULL;

    expect_any(__wrap_connect, sockfd);
    expect_any(__wrap_connect, addr);
    expect_any(__wrap_connect, addrlen);
    will_return(__wrap_connect, 0);
    assert_int_equal(imtp_connection_connect(&icon, NULL, uri_server), 0);

    create_dummy_frame(&frame_send);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, 8);
    will_return(__wrap_read, 11);
    will_return(__wrap_read, 8);

    expect_any(__wrap_read, fd);
    expect_any(__wrap_read, buf);
    expect_value(__wrap_read, count, frame_send->length);
    will_return(__wrap_read, 12);
    will_return(__wrap_read, frame_send->length);

    assert_int_not_equal(imtp_connection_read_frame(NULL, NULL), 0);
    assert_int_not_equal(imtp_connection_read_frame(icon, NULL), 0);
    assert_int_equal(imtp_connection_read_frame(icon, &frame_recv), 0);
    assert_non_null(frame_recv);
    assert_int_equal(frame_send->length, frame_recv->length);
    assert_non_null(frame_recv->tlv);

    assert_int_equal(frame_recv->tlv->length, frame_send->tlv->length);

    imtp_frame_delete(&frame_recv);
    imtp_frame_delete(&frame_send);
    imtp_connection_delete(&icon);
}
