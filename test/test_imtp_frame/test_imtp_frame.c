/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <yajl/yajl_gen.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxj/amxj_variant.h>

#include "imtp/imtp_frame.h"
#include "test_imtp_frame.h"
#include "imtp_priv.h"

void test_can_convert_frame_to_bytes_and_back(UNUSED void** state) {
    imtp_frame_t* frame = NULL;
    imtp_frame_t* frame_recv = NULL;
    imtp_tlv_t* tlv_1 = NULL;
    imtp_tlv_t* tlv_2 = NULL;
    imtp_tlv_t* tlv_3 = NULL;
    const imtp_tlv_t* tlv_recv_1 = NULL;
    const imtp_tlv_t* tlv_recv_2 = NULL;
    const imtp_tlv_t* tlv_recv_3 = NULL;
    char* topic_1 = "Hello";
    char* topic_2 = "World";
    char* topic_3 = "Ambiorix";
    uint32_t len_1 = strlen(topic_1);
    uint32_t len_2 = strlen(topic_2);
    uint32_t len_3 = strlen(topic_3);
    void* bytes = NULL;

    assert_int_equal(imtp_frame_new(&frame), 0);
    assert_int_equal(imtp_tlv_new(&tlv_1, imtp_tlv_type_topic, len_1, topic_1, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_new(&tlv_2, imtp_tlv_type_topic, len_2, topic_2, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_new(&tlv_3, imtp_tlv_type_topic, len_3, topic_3, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_frame_tlv_add(frame, tlv_1), 0);
    assert_int_equal(imtp_frame_tlv_add(frame, tlv_2), 0);
    assert_int_equal(imtp_frame_tlv_add(frame, tlv_3), 0);
    assert_int_equal(imtp_frame_to_bytes(frame, &bytes), 0);

    assert_int_equal(imtp_frame_parse(&frame_recv, bytes), 0);
    assert_int_equal(frame->length, frame_recv->length);
    assert_non_null(frame_recv->tlv);

    tlv_recv_1 = frame_recv->tlv;
    assert_int_equal(strncmp(topic_1, tlv_recv_1->value + tlv_recv_1->offset, len_1), 0);

    tlv_recv_2 = imtp_message_get_next_tlv(tlv_recv_1, imtp_tlv_type_topic);
    assert_non_null(tlv_recv_2);
    assert_int_equal(strncmp(topic_2, tlv_recv_2->value + tlv_recv_2->offset, len_2), 0);

    tlv_recv_3 = imtp_message_get_next_tlv(tlv_recv_2, imtp_tlv_type_topic);
    assert_non_null(tlv_recv_3);
    assert_int_equal(strncmp(topic_3, tlv_recv_3->value + tlv_recv_3->offset, len_3), 0);

    imtp_frame_delete(&frame_recv);
    imtp_frame_delete(&frame);
}

void test_can_handle_null_pointers(UNUSED void** state) {
    imtp_frame_t* frame = NULL;
    assert_int_not_equal(imtp_frame_new(NULL), 0);
    imtp_frame_delete(NULL);
    imtp_frame_delete(&frame);

    assert_int_equal(imtp_frame_new(&frame), 0);
    assert_int_not_equal(imtp_frame_tlv_add(NULL, NULL), 0);
    assert_int_not_equal(imtp_frame_tlv_add(frame, NULL), 0);
    assert_int_not_equal(imtp_frame_to_bytes(NULL, NULL), 0);
    assert_int_not_equal(imtp_frame_to_bytes(frame, NULL), 0);
    imtp_frame_delete(&frame);

    assert_int_not_equal(imtp_frame_parse(NULL, NULL), 0);
    assert_int_not_equal(imtp_frame_parse(&frame, NULL), 0);
    assert_null(imtp_frame_get_first_tlv(NULL, imtp_tlv_type_handshake));
}

void test_can_get_tlv_from_frame(UNUSED void** state) {
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* tlv_1 = NULL;
    imtp_tlv_t* tlv_2 = NULL;
    const imtp_tlv_t* tlv_fetched = NULL;
    char* topic_1 = "Hello";
    char* topic_2 = "World";
    uint32_t len_1 = strlen(topic_1);
    uint32_t len_2 = strlen(topic_2);

    assert_int_equal(imtp_frame_new(&frame), 0);
    assert_int_equal(imtp_tlv_new(&tlv_1, imtp_tlv_type_topic, len_1, topic_1, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_new(&tlv_2, imtp_tlv_type_topic, len_2, topic_2, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_frame_tlv_add(frame, tlv_1), 0);
    assert_int_equal(imtp_frame_tlv_add(frame, tlv_2), 0);
    tlv_fetched = imtp_frame_get_first_tlv(frame, imtp_tlv_type_topic);
    assert_non_null(tlv_fetched);
    assert_int_equal(strncmp(topic_1, tlv_fetched->value + tlv_fetched->offset, len_1), 0);
    tlv_fetched = imtp_message_get_next_tlv(tlv_fetched, imtp_tlv_type_topic);
    assert_int_equal(strncmp(topic_2, tlv_fetched->value + tlv_fetched->offset, len_2), 0);

    imtp_frame_delete(&frame);
}
