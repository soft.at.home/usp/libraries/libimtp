/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include "imtp_dummy.h"

void PRIVATE create_dummy_tlv(imtp_tlv_t** tlv) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    char topic[] = "Hello-test";
    uint32_t len_head = 0;
    uint32_t len_topic = strlen(topic);

    imtp_tlv_new(&tlv_head, imtp_tlv_type_head, len_head, NULL, 0, IMTP_TLV_TAKE);
    imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY);
    imtp_tlv_add(tlv_head, tlv_topic);

    *tlv = tlv_head;
}

void PRIVATE create_dummy_frame(imtp_frame_t** frame) {
    imtp_tlv_t* tlv_1 = NULL;
    imtp_tlv_t* tlv_2 = NULL;
    imtp_tlv_t* tlv_3 = NULL;
    char* topic_1 = "Hello";
    char* topic_2 = "World";
    char bytes[4] = { 0, 1, 2, 3};
    uint32_t len_1 = strlen(topic_1);
    uint32_t len_2 = strlen(topic_2);
    uint32_t len_3 = 4;

    imtp_frame_new(frame);
    imtp_tlv_new(&tlv_1, imtp_tlv_type_topic, len_1, topic_1, 0, IMTP_TLV_COPY);
    imtp_tlv_new(&tlv_2, imtp_tlv_type_topic, len_2, topic_2, 0, IMTP_TLV_COPY);
    imtp_tlv_new(&tlv_3, imtp_tlv_type_protobuf_bytes, len_3, bytes, 0, IMTP_TLV_COPY);
    imtp_frame_tlv_add(*frame, tlv_1);
    imtp_frame_tlv_add(*frame, tlv_2);
    imtp_frame_tlv_add(*frame, tlv_3);
}
