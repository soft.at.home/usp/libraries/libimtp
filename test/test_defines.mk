MACHINE = $(shell $(CC) -dumpmachine)

SRCDIR = $(realpath ../../src)
DUMMYDIR = $(realpath ../dummy_functions)
OBJDIR = $(realpath ../../output/$(MACHINE)/coverage)
INCDIR = $(realpath ../../include ../../include_priv ../include/imtp ../dummy_functions)

HEADERS = $(wildcard $(INCDIR)/imtp/*.h)
SOURCES = $(wildcard $(SRCDIR)/*.c) $(wildcard $(DUMMYDIR)/*.c)

MOCK_WRAP = connect \
			sendto \
			recvfrom \
			recv \
			accept \
			read \
			write \
			sendmsg \
			recvmsg

WRAP_FUNC=-Wl,--wrap=

CFLAGS += -Werror -Wall -Wextra -Wno-attributes\
          --std=gnu99 -g3 -Wmissing-declarations \
		  $(addprefix -I ,$(INCDIR)) -I$(OBJDIR)/.. -I../socket_mock/ -I../dummy_functions \
		  -fkeep-inline-functions -fkeep-static-functions -Wno-format-nonliteral \
		  $(shell pkg-config --cflags cmocka)

LDFLAGS += -fkeep-inline-functions -fkeep-static-functions \
		   $(shell pkg-config --libs cmocka) -lamxc -lamxj -luriparser -lsahtrace

LDFLAGS += -g $(addprefix $(WRAP_FUNC),$(MOCK_WRAP))
