/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <cmocka.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include "imtp/imtp_message.h"
#include "imtp_dummy.h"
#include "socket_mock.h"
#include "imtp_priv.h"

int __wrap_connect(int sockfd, const struct sockaddr* addr, socklen_t addrlen) {
    check_expected(sockfd);
    check_expected_ptr(addr);
    check_expected(addrlen);
    if(addr->sa_data == NULL) {
        return -1;
    }
    if(strlen(addr->sa_data) == 0) {
        return -1;
    }

    return mock_type(int);
}

int __wrap_accept(int sockfd, struct sockaddr* addr, socklen_t* addrlen) {
    check_expected(sockfd);
    check_expected_ptr(addr);
    check_expected_ptr(addrlen);

    if(sockfd < 0) {
        return -1;
    }

    return mock_type(int);
}

int __wrap_sendto(int sockfd, const void* buf, size_t len, int flags,
                  const struct sockaddr* dest_addr, socklen_t addrlen) {
    check_expected(sockfd);
    check_expected_ptr(buf);
    check_expected(len);
    check_expected(flags);
    check_expected_ptr(dest_addr);
    check_expected(addrlen);

    return mock_type(int);
}

ssize_t __wrap_recv(int sockfd, void* buf, size_t len, int flags) {
    check_expected(sockfd);
    check_expected_ptr(buf);
    check_expected(len);
    check_expected(flags);
    int rv = 0;
    imtp_tlv_t* tlv = NULL;
    void* bytes = NULL;
    uint8_t* ptr = (uint8_t*) buf;
    uint32_t len_nbo = 0;

    create_dummy_tlv(&tlv);
    imtp_message_to_bytes(tlv, &bytes);
    assert_non_null(bytes);
    // Copy part of TLV message to output buffer
    rv = mock_type(int);
    switch(rv) {
    case 0:
        goto exit;
    case 1:
        memcpy(ptr, bytes, sizeof(uint8_t));
        ptr += sizeof(uint8_t);
        len_nbo = htonl(tlv->length);
        memcpy(ptr, &len_nbo, sizeof(uint32_t));
        break;
    case 2:
        memcpy(ptr, bytes + sizeof(uint8_t) + sizeof(uint32_t), tlv->length);
        break;
    default:
        break;
    }

exit:
    free(bytes);
    imtp_tlv_delete(&tlv);

    return mock_type(int);
}

int __wrap_recvfrom(int sockfd, const void* buf, size_t len, int flags,
                    const struct sockaddr* dest_addr, socklen_t* addrlen) {
    check_expected(sockfd);
    check_expected_ptr(buf);
    check_expected(len);
    check_expected(flags);
    check_expected_ptr(dest_addr);
    check_expected_ptr(addrlen);
    int rv = 0;
    imtp_tlv_t* tlv = NULL;
    void* bytes = NULL;
    uint8_t* ptr = (uint8_t*) buf;
    int bytes_to_copy = 0;
    int offset = 0;
    uint32_t len_nbo = 0;

    create_dummy_tlv(&tlv);
    imtp_message_to_bytes(tlv, &bytes);
    assert_non_null(bytes);
    // Copy part of TLV message to output buffer
    rv = mock_type(int);
    switch(rv) {
    case 0:
        goto exit;
    case 1:
        memcpy(ptr, bytes, sizeof(uint8_t));
        ptr += sizeof(uint8_t);
        len_nbo = htonl(tlv->length);
        memcpy(ptr, &len_nbo, sizeof(uint32_t));
        break;
    case 2:
        memcpy(ptr, bytes + sizeof(uint8_t) + sizeof(uint32_t), tlv->length);
        break;
    case 3:
        bytes_to_copy = mock_type(int);
        offset = mock_type(int);
        memcpy(ptr, bytes + sizeof(uint8_t) + sizeof(uint32_t) + offset, bytes_to_copy);
    default:
        break;
    }

exit:
    free(bytes);
    imtp_tlv_delete(&tlv);

    return mock_type(int);
}

int __wrap_write(int fd, void* buf, size_t count) {
    check_expected(fd);
    check_expected_ptr(buf);
    check_expected(count);

    return mock_type(int);
}

int __wrap_read(int fd, void* buf, size_t count) {
    check_expected(fd);
    check_expected_ptr(buf);
    check_expected(count);
    imtp_frame_t* frame = NULL;
    imtp_tlv_t* tlv = NULL;
    int rv = 0;
    void* bytes = NULL;
    uint8_t* ptr = (uint8_t*) buf;
    int bytes_to_copy = 0;
    int offset = 0;
    uint32_t len_nbo = 0;
    uint32_t len_sync = strlen(IMTP_SYNC_USP);

    rv = mock_type(int);
    if(rv < 10) {
        create_dummy_tlv(&tlv);
        imtp_message_to_bytes(tlv, &bytes);
    } else {
        create_dummy_frame(&frame);
        imtp_frame_to_bytes(frame, &bytes);
    }
    assert_non_null(bytes);

    // Copy part of TLV message to output buffer
    switch(rv) {
    case 0:
        goto exit;
    case 1:
        memcpy(ptr, bytes, sizeof(uint8_t));
        ptr += sizeof(uint8_t);
        len_nbo = htonl(tlv->length);
        memcpy(ptr, &len_nbo, sizeof(uint32_t));
        break;
    case 2:
        memcpy(ptr, bytes + sizeof(uint8_t) + sizeof(uint32_t), tlv->length);
        break;
    case 3:
        bytes_to_copy = mock_type(int);
        offset = mock_type(int);
        memcpy(ptr, bytes + sizeof(uint8_t) + sizeof(uint32_t) + offset, bytes_to_copy);
        break;
    case 11:
        memcpy(ptr, bytes, len_sync);
        ptr += len_sync;
        len_nbo = htonl(frame->length);
        memcpy(ptr, &len_nbo, sizeof(uint32_t));
        break;
    case 12:
        memcpy(ptr, bytes + len_sync + sizeof(uint32_t), frame->length);
        break;
    default:
        break;
    }

exit:
    free(bytes);
    imtp_frame_delete(&frame);
    imtp_tlv_delete(&tlv);

    return mock_type(int);
}

ssize_t __wrap_sendmsg(int fd, const struct msghdr* message, int flags) {
    struct cmsghdr* cmsg = NULL;
    int send_fd = -1;

    check_expected(fd);
    check_expected(message);
    check_expected(flags);

    cmsg = CMSG_FIRSTHDR(message);
    assert_non_null(cmsg);

    memcpy(&send_fd, CMSG_DATA(cmsg), sizeof(int));
    check_expected(send_fd);

    return mock_type(int);
}

ssize_t __wrap_recvmsg(int fd, struct msghdr* message, int flags) {
    struct cmsghdr* cmsg;
    int recv_fd = -1;

    check_expected(fd);
    check_expected(message);
    check_expected(flags);

    recv_fd = mock_type(int);

    cmsg = CMSG_FIRSTHDR(message);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(recv_fd));
    memcpy(CMSG_DATA(cmsg), &recv_fd, sizeof(recv_fd));

    return mock_type(int);
}



