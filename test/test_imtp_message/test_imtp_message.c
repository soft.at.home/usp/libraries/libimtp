/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include <yajl/yajl_gen.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxj/amxj_variant.h>

#include "imtp/imtp_message.h"
#include "test_imtp_message.h"
#include "imtp_priv.h"

static bool test_verify_data(const amxc_var_t* data, const char* field, const char* value) {
    bool rv = false;
    char* field_value = NULL;
    amxc_var_t* field_data = GETP_ARG(data, field);

    printf("Verify table data: check field [%s] contains [%s]\n", field, value);
    fflush(stdout);
    assert_non_null(field_data);

    field_value = amxc_var_dyncast(cstring_t, field_data);
    assert_non_null(field_data);

    rv = (strcmp(field_value, value) == 0);

    free(field_value);
    return rv;
}

void test_can_create_and_delete_imtp_tlv(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    char* topic = "Hello-test";
    uint32_t len_head = 0;
    uint32_t len_topic = strlen(topic);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, len_head, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_non_null(tlv_head);
    assert_int_equal(tlv_head->type, imtp_tlv_type_head);
    assert_int_equal(tlv_head->length, len_head);
    assert_null(tlv_head->value);
    assert_null(tlv_head->next);

    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    assert_non_null(tlv_topic);
    assert_int_equal(tlv_topic->type, imtp_tlv_type_topic);
    assert_int_equal(tlv_topic->length, len_topic);
    assert_non_null(tlv_topic->value);
    assert_int_equal(strncmp(tlv_topic->value, topic, len_topic), 0);
    assert_null(tlv_topic->next);

    imtp_tlv_delete(&tlv_head);
    assert_null(tlv_head);
    imtp_tlv_delete(&tlv_topic);
    assert_null(tlv_topic);
    return;
}

void test_imtp_tlv_new_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv = NULL;
    char* value = "Hello-test";
    uint32_t len = strlen(value);

    // Test tlv == NULL
    assert_int_equal(imtp_tlv_new(NULL, imtp_tlv_type_topic, len, NULL, 0, IMTP_TLV_COPY), -1);
    assert_null(tlv);

    // Test invalid value
    assert_int_equal(imtp_tlv_new(&tlv, imtp_tlv_type_topic, len, NULL, 0, IMTP_TLV_COPY), -1);
    assert_null(tlv);

    // Test invalid type
    assert_int_equal(imtp_tlv_new(&tlv, imtp_tlv_type_max, len, value, 0, IMTP_TLV_COPY), -1);
    assert_null(tlv);

    // Test invalid FLAGS
    assert_int_equal(imtp_tlv_new(&tlv, imtp_tlv_type_topic, len, value, 0, 1234), -1);
    assert_null(tlv);

    imtp_tlv_delete(&tlv);
    assert_null(tlv);

    return;
}

void test_imtp_tlv_delete_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv = NULL;

    imtp_tlv_delete(NULL);
    imtp_tlv_delete(&tlv);

    return;
}

void test_imtp_tlv_add(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    char* topic = "Hello-test";
    uint32_t len_head = 0;
    uint32_t len_topic = strlen(topic);
    uint32_t total_len = 0;

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, len_head, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);

    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic), 0);
    total_len += strlen(topic) + 2 * sizeof(uint32_t);
    assert_int_equal(tlv_head->next, tlv_topic);
    assert_null(tlv_topic->next);

    imtp_tlv_delete(&tlv_head);
    return;
}

void test_imtp_tlv_add_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    char* topic = "Hello-test";
    uint32_t len_head = 0;
    uint32_t len_topic = strlen(topic);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, len_head, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_add(NULL, NULL), -1);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic), -1);
    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic), 0);

    imtp_tlv_delete(&tlv_head);
    return;
}

void test_imtp_get_first_tlv(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    const imtp_tlv_t* tlv_topic_get = NULL;
    char* topic = "Hello-test";
    uint32_t len_head = 0;
    uint32_t len_topic = strlen(topic);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, len_head, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic), 0);

    tlv_topic_get = imtp_message_get_first_tlv(tlv_head, imtp_tlv_type_topic);
    assert_non_null(tlv_topic_get);
    assert_int_equal(strncmp(tlv_topic_get->value, tlv_topic->value, len_topic), 0);

    imtp_tlv_delete(&tlv_head);
    return;
}

void test_imtp_get_first_tlv_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv_topic = NULL;
    const imtp_tlv_t* tlv_topic_get = NULL;
    char* topic = "Hello-test";
    uint32_t len_topic = strlen(topic);

    tlv_topic_get = imtp_message_get_first_tlv(tlv_topic, imtp_tlv_type_topic);
    assert_null(tlv_topic_get);

    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    tlv_topic_get = imtp_message_get_first_tlv(tlv_topic, imtp_tlv_type_topic);
    assert_null(tlv_topic_get);

    imtp_tlv_delete(&tlv_topic);
    return;
}

void test_imtp_get_next_tlv(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic_1 = NULL;
    imtp_tlv_t* tlv_topic_2 = NULL;
    const imtp_tlv_t* tlv_topic_1_get = NULL;
    const imtp_tlv_t* tlv_topic_2_get = NULL;
    char* topic_1 = "Hello-test-one";
    char* topic_2 = "Hello-test-two";
    uint32_t len_topic_1 = strlen(topic_1);
    uint32_t len_topic_2 = strlen(topic_2);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic_1, imtp_tlv_type_topic, len_topic_1, topic_1, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic_2, imtp_tlv_type_topic, len_topic_2, topic_2, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic_1), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic_2), 0);

    tlv_topic_1_get = imtp_message_get_next_tlv(tlv_head, imtp_tlv_type_topic);
    assert_non_null(tlv_topic_1_get);
    assert_int_equal(strncmp(tlv_topic_1_get->value, tlv_topic_1->value, len_topic_1), 0);

    tlv_topic_2_get = imtp_message_get_next_tlv(tlv_topic_1_get, imtp_tlv_type_topic);
    assert_non_null(tlv_topic_2_get);
    assert_int_equal(strncmp(tlv_topic_2_get->value, tlv_topic_2->value, len_topic_2), 0);

    imtp_tlv_delete(&tlv_head);
    return;
}

void test_imtp_get_next_tlv_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv_topic = NULL;
    imtp_tlv_t* tlv_head = NULL;
    const imtp_tlv_t* tlv_topic_get = NULL;
    char* topic = "Hello-test";
    uint32_t len_topic = strlen(topic);

    tlv_topic_get = imtp_message_get_next_tlv(tlv_topic, imtp_tlv_type_topic);
    assert_null(tlv_topic_get);

    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    tlv_topic->type = imtp_tlv_type_max;
    tlv_topic_get = imtp_message_get_next_tlv(tlv_topic, imtp_tlv_type_topic);
    assert_null(tlv_topic_get);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    tlv_topic_get = imtp_message_get_next_tlv(tlv_head, imtp_tlv_type_topic);

    imtp_tlv_delete(&tlv_topic);
    imtp_tlv_delete(&tlv_head);
    return;
}

void test_imtp_can_serialize_tlv(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    char* topic = "Hello-test";
    uint32_t len_topic = strlen(topic);
    void* bytes = NULL;

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic), 0);

    assert_int_equal(imtp_message_to_bytes(tlv_head, &bytes), 0);

    imtp_tlv_delete(&tlv_head);
    free(bytes);
    return;
}

void test_imtp_message_to_bytes_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    char* topic = "Hello-test";
    uint32_t len_topic = strlen(topic);
    void* bytes = NULL;

    assert_int_equal(imtp_message_to_bytes(tlv_head, &bytes), -1);
    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_message_to_bytes(tlv_topic, &bytes), -1);
    assert_int_equal(imtp_message_to_bytes(tlv_topic, NULL), -1);

    imtp_tlv_delete(&tlv_head);
    imtp_tlv_delete(&tlv_topic);
    return;
}

void test_imtp_message_parse(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic_1 = NULL;
    imtp_tlv_t* tlv_topic_2 = NULL;
    imtp_tlv_t* tlv_unpack = NULL;
    imtp_tlv_t* tlv_next = NULL;
    char* topic_1 = "Hello-test";
    char* topic_2 = "Hello-test-other";
    uint32_t len_topic_1 = strlen(topic_1);
    uint32_t len_topic_2 = strlen(topic_2);
    void* bytes = NULL;

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic_1, imtp_tlv_type_topic, len_topic_1, topic_1, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic_2, imtp_tlv_type_topic, len_topic_2, topic_2, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic_1), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic_2), 0);
    assert_int_equal(imtp_message_to_bytes(tlv_head, &bytes), 0);
    assert_non_null(bytes);

    assert_int_equal(imtp_message_parse(&tlv_unpack, bytes), 0);
    assert_non_null(tlv_unpack->value);
    assert_non_null(tlv_unpack->next);

    tlv_next = tlv_unpack->next;
    assert_int_equal(tlv_next->type, imtp_tlv_type_topic);
    assert_int_equal(tlv_next->length, strlen(topic_1));
    assert_int_equal(tlv_next->offset, 2 * sizeof(uint8_t) + 2 * sizeof(uint32_t));
    assert_int_equal(strncmp(topic_1, tlv_next->value + tlv_next->offset, len_topic_1), 0);

    tlv_next = tlv_next->next;
    assert_int_equal(tlv_next->type, imtp_tlv_type_topic);
    assert_int_equal(tlv_next->length, strlen(topic_2));
    assert_int_equal(tlv_next->offset, len_topic_1 + 3 * sizeof(uint8_t) + 3 * sizeof(uint32_t));
    assert_int_equal(strncmp(topic_2, tlv_next->value + tlv_next->offset, len_topic_2), 0);

    imtp_tlv_delete(&tlv_head);
    imtp_tlv_delete(&tlv_unpack);
    return;
}

void test_imtp_message_parse_invalid(UNUSED void** state) {
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_topic = NULL;
    imtp_tlv_t* tlv_unpack = NULL;
    char* topic = "Hello-test";
    uint32_t len_topic = strlen(topic);
    void* bytes = NULL;

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_topic, imtp_tlv_type_topic, len_topic, topic, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_topic), 0);
    assert_int_equal(imtp_message_to_bytes(tlv_head, &bytes), 0);

    assert_int_equal(imtp_message_parse(NULL, bytes), -1);
    assert_int_equal(imtp_message_parse(&tlv_unpack, NULL), -1);

    imtp_tlv_delete(&tlv_head);
    free(bytes);
    imtp_tlv_delete(&tlv_unpack);
    return;
}

void test_imtp_tlv_type_json(UNUSED void** state) {
    amxc_var_t table;
    amxc_var_t result_table;
    const char* json_str = NULL;
    imtp_tlv_t* tlv_head = NULL;
    imtp_tlv_t* tlv_json = NULL;
    imtp_tlv_t* tlv_unpack = NULL;
    imtp_tlv_t* tlv_next = NULL;
    void* bytes = NULL;

    amxc_var_init(&result_table);
    amxc_var_init(&table);
    amxc_var_set_type(&table, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &table, "text", "pancakes");
    amxc_var_add_key(uint32_t, &table, "number", 456);
    amxc_var_add_key(bool, &table, "permission", false);
    assert_int_equal(amxc_var_cast(&table, AMXC_VAR_ID_JSON), 0);

    json_str = amxc_var_constcast(jstring_t, &table);
    assert_non_null(json_str);

    assert_int_equal(imtp_tlv_new(&tlv_head, imtp_tlv_type_head, 0, NULL, 0, IMTP_TLV_TAKE), 0);
    assert_int_equal(imtp_tlv_new(&tlv_json, imtp_tlv_type_json, strlen(json_str),
                                  (char*) json_str, 0, IMTP_TLV_COPY), 0);
    assert_int_equal(imtp_tlv_add(tlv_head, tlv_json), 0);
    assert_int_equal(imtp_message_to_bytes(tlv_head, &bytes), 0);
    assert_non_null(bytes);

    assert_int_equal(imtp_message_parse(&tlv_unpack, bytes), 0);
    assert_non_null(tlv_unpack->value);
    assert_non_null(tlv_unpack->next);

    tlv_next = tlv_unpack->next;
    assert_int_equal(tlv_next->type, imtp_tlv_type_json);
    assert_int_equal(tlv_next->length, strlen(json_str));
    assert_int_equal(tlv_next->offset, 2 * sizeof(uint8_t) + 2 * sizeof(uint32_t));
    assert_int_equal(strncmp(json_str, tlv_next->value + tlv_next->offset, strlen(json_str)), 0);

    assert_int_equal(amxc_var_set(jstring_t, &result_table, json_str), 0);
    amxc_var_cast(&result_table, AMXC_VAR_ID_ANY);
    amxc_var_dump(&result_table, STDOUT_FILENO);
    assert_true(test_verify_data(&result_table, "text", "pancakes"));
    assert_true(test_verify_data(&result_table, "number", "456"));
    assert_true(test_verify_data(&result_table, "permission", "false"));

    imtp_tlv_delete(&tlv_head);
    imtp_tlv_delete(&tlv_unpack);
    amxc_var_clean(&result_table);
    amxc_var_clean(&table);
}
