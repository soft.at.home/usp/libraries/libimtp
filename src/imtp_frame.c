/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

#include <amxc/amxc_macros.h>
#include <debug/sahtrace.h>

#include "imtp_priv.h"
#include "imtp/imtp_message.h"
#include "imtp/imtp_frame.h"

static int imtp_frame_check_sync_bytes(void* bmsg, const char* sync_bytes) {
    int retval = 0;
    size_t len = strlen(sync_bytes);
    char* recv_bytes = (char*) calloc(1, len + 1);

    when_null_status(recv_bytes, exit, retval = -1);

    memcpy(recv_bytes, bmsg, len);
    when_true_status(strcmp(sync_bytes, recv_bytes) != 0, exit, retval = -1);

    retval = len;
exit:
    free(recv_bytes);
    return retval;
}

int imtp_frame_new(imtp_frame_t** frame) {
    int retval = -1;

    when_null(frame, exit);

    *frame = (imtp_frame_t*) calloc(1, sizeof(imtp_frame_t));
    when_null(*frame, exit);

    retval = 0;
exit:
    return retval;
}

void imtp_frame_delete(imtp_frame_t** frame) {

    when_null(frame, exit);
    when_null(*frame, exit);

    imtp_tlv_delete(&(*frame)->tlv);
    (*frame)->tlv = NULL;

    free(*frame);
    *frame = NULL;

exit:
    return;
}

int imtp_frame_tlv_add(imtp_frame_t* frame, imtp_tlv_t* tlv_new) {
    int retval = -1;
    imtp_tlv_t* tlv_last = NULL;

    when_null(frame, exit);
    when_null(tlv_new, exit);

    if(frame->tlv == NULL) {
        frame->tlv = tlv_new;
        frame->length += tlv_new->length + sizeof(uint8_t) + sizeof(uint32_t);
        retval = 0;
        goto exit;
    }

    tlv_last = frame->tlv;
    while(tlv_last->next != NULL) {
        tlv_last = tlv_last->next;
    }
    tlv_last->next = tlv_new;

    frame->length += tlv_new->length + sizeof(uint8_t) + sizeof(uint32_t);

    retval = 0;
exit:
    return retval;
}

int imtp_frame_to_bytes(imtp_frame_t* frame, void** bmsg) {
    int retval = -1;
    imtp_tlv_t* tlv_next = NULL;
    uint8_t* ptr = NULL;
    uint32_t len = strlen(IMTP_SYNC_USP);
    uint32_t len_nbo = 0;

    when_null(frame, exit);
    when_null(bmsg, exit);

    *bmsg = calloc(1, len + sizeof(uint32_t) + frame->length);
    when_null(*bmsg, exit);

    ptr = (uint8_t*) (*bmsg);

    memcpy(ptr, IMTP_SYNC_USP, len);
    ptr += len;
    len_nbo = htonl(frame->length);
    memcpy(ptr, &len_nbo, sizeof(uint32_t));
    ptr += sizeof(uint32_t);

    tlv_next = frame->tlv;
    while(tlv_next != NULL) {
        imtp_tlv_serialize(tlv_next, ptr);
        ptr += tlv_next->length + sizeof(uint8_t) + sizeof(uint32_t);
        tlv_next = tlv_next->next;
    }

    retval = 0;
exit:
    return retval;
}

int imtp_frame_parse(imtp_frame_t** frame, void* bmsg) {
    int retval = -1;
    uint8_t* ptr = NULL;
    uint32_t processed_len = 0;
    uint32_t total_len = 0;
    uint32_t len_nbo = 0;
    int offset = 0;

    when_null(frame, exit);
    when_null(bmsg, exit);

    offset = imtp_frame_check_sync_bytes(bmsg, IMTP_SYNC_USP);
    when_true(offset < 0, exit);

    retval = imtp_frame_new(frame);
    when_failed(retval, exit);

    ptr = (uint8_t*) bmsg + (uint32_t) offset;
    memcpy(&len_nbo, ptr, sizeof(uint32_t));
    total_len = ntohl(len_nbo);
    ptr += sizeof(uint32_t);

    processed_len = sizeof(uint32_t);
    // Read out all incoming TLVs
    while(processed_len < total_len) {
        imtp_tlv_t* tlv_next = imtp_message_read_next_tlv(ptr, processed_len + offset, bmsg);
        imtp_frame_tlv_add(*frame, tlv_next);
        ptr += tlv_next->length + sizeof(uint8_t) + sizeof(uint32_t);
        processed_len += tlv_next->length + sizeof(uint8_t) + sizeof(uint32_t);
    }

    retval = 0;
exit:
    return retval;
}

const imtp_tlv_t* imtp_frame_get_first_tlv(const imtp_frame_t* frame, imtp_tlv_type_t type) {
    const imtp_tlv_t* tlv_res = NULL;

    when_null(frame, exit);
    when_null(frame->tlv, exit);

    if(frame->tlv->type == type) {
        tlv_res = frame->tlv;
    } else {
        tlv_res = imtp_message_get_next_tlv(frame->tlv, type);
    }

exit:
    return tlv_res;
}
