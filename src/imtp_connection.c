/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>
#include <uriparser/Uri.h>

#include <amxc/amxc_macros.h>

#include "imtp/imtp_connection.h"
#include "imtp_connection_priv.h"
#include "imtp_priv.h"

#define MAX_CONNECTIONS 100

static amxc_llist_t con_list;

static int imtp_connection_empty(imtp_connection_t** icon) {
    int retval = -1;

    when_null(icon, exit);
    (*icon) = (imtp_connection_t*) calloc(1, sizeof(imtp_connection_t));
    when_null(*icon, exit);

    retval = 0;
exit:
    return retval;
}

static void imtp_connection_it_delete(amxc_llist_it_t* it) {
    imtp_connection_t* con = amxc_llist_it_get_data(it, imtp_connection_t, it);
    imtp_connection_delete(&con);
}

CONSTRUCTOR static void imtp_llist_init(void) {
    amxc_llist_init(&con_list);
}

DESTRUCTOR static void imtp_llist_cleanup(void) {
    amxc_llist_clean(&con_list, imtp_connection_it_delete);
}

static int imtp_uri_part_to_string(amxc_string_t* buffer,
                                   UriTextRangeA* tr) {

    return amxc_string_append(buffer, tr->first, tr->afterLast - tr->first);
}

static int imtp_uri_path_to_string(amxc_string_t* buffer,
                                   UriPathSegmentA* ps) {

    const char* sep = "/";
    while(ps) {
        amxc_string_append(buffer, sep, strlen(sep));
        imtp_uri_part_to_string(buffer, &ps->text);
        sep = "/";
        ps = ps->next;
    }

    return 0;
}

/* Configure a client side datagram socket that will be used to connect to a server side socket */
static int imtp_connection_client_dgram_socket(imtp_connection_t* icon, const char* from_uri) {
    int retval = -1;
    char* from_path = NULL;

    imtp_uri_parse(from_uri, NULL, NULL, NULL, &from_path);
    when_null(from_path, exit);

    icon->addr_self = (struct sockaddr_un*) calloc(1, sizeof(struct sockaddr_un));
    when_null(icon->addr_self, exit);

    SAH_TRACEZ_INFO(ME, "Creating datagram socket on path %s", from_path);
    icon->fd = socket(AF_UNIX, SOCK_DGRAM | SOCK_NONBLOCK, 0);
    when_true(icon->fd < 0, exit);
    icon->flags = SOCK_DGRAM;

    icon->addr_self->sun_family = AF_UNIX;
    strncpy(icon->addr_self->sun_path, from_path, sizeof(icon->addr_self->sun_path) - 1);

    retval = bind(icon->fd, (struct sockaddr*) icon->addr_self, sizeof(struct sockaddr_un));
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error binding datagram socket on path = %s", from_path);
    }
    when_failed(retval, exit);

    retval = 0;
exit:
    free(from_path);
    return retval;
}

/* Configure a client side stream socket that will be used to connect to a server side socket */
static int imtp_connection_client_stream_socket(imtp_connection_t* icon) {
    int retval = -1;

    SAH_TRACEZ_INFO(ME, "Creating client stream socket");
    icon->fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    when_true(icon->fd < 0, exit);
    icon->flags = SOCK_STREAM;

    retval = 0;
exit:
    return retval;
}

/* Reset the read buffer and read length */
static void imtp_buffer_reset(imtp_connection_t* icon) {
    when_null(icon, exit);

    free(icon->buffer);
    icon->buffer = NULL;
    icon->read_len = 0;

exit:
    return;
}

/**
   Returns:
    0 in case len bytes could be read,
    1 in case at least 1 byte, but fewer than len bytes could be read
    -1 in case no bytes could be read (broken connection)
 */
static int imtp_connection_read_part(imtp_connection_t* icon, int len) {
    int retval = -1;
    int read_len = 0;

    if(icon->flags & SOCK_DGRAM) {
        socklen_t addrlen = sizeof(struct sockaddr_un);
        read_len = recvfrom(icon->fd, icon->buffer + icon->read_len, len, MSG_DONTWAIT,
                            (struct sockaddr*) icon->addr_other, &addrlen);
    } else if(icon->flags & SOCK_STREAM) {
        read_len = read(icon->fd, icon->buffer + icon->read_len, len);
    }
    if(read_len <= 0) {
        imtp_buffer_reset(icon);
        retval = -1;
        goto exit;
    }

    icon->read_len += read_len;
    if(read_len < len) {
        retval = 1;
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

static int imtp_connection_read_impl(imtp_connection_t* icon, int prefix_size) {
    int retval = -1;
    int length_size = sizeof(uint32_t);
    uint32_t len_nbo = 0;
    uint32_t msg_len = 0;

    if(icon->buffer == NULL) {
        icon->buffer = calloc(1, prefix_size + length_size);
    }

    if(icon->read_len < prefix_size + length_size) {
        retval = imtp_connection_read_part(icon, prefix_size + length_size - icon->read_len);
        when_failed(retval, exit);
    }

    memcpy(&len_nbo, icon->buffer + prefix_size, length_size);
    msg_len = ntohl(len_nbo);

    // TLVs that follow must have at least a type(1) + length(4)
    if(msg_len < 5) {
        retval = -1;
        imtp_buffer_reset(icon);
        goto exit;
    }

    // Only realloc memory once; not if this was already done previously
    if(icon->read_len == prefix_size + length_size) {
        uint8_t* ptr = (uint8_t*) realloc(icon->buffer, prefix_size + length_size + msg_len);
        if(ptr == NULL) {
            SAH_TRACEZ_ERROR(ME, "Unable to realloc %d bytes", prefix_size + length_size + msg_len);
            imtp_buffer_reset(icon);
            retval = -1;
            goto exit;
        } else {
            icon->buffer = ptr;
        }
    }

    retval = imtp_connection_read_part(icon, msg_len - icon->read_len + prefix_size + length_size);
    when_failed(retval, exit);

exit:
    return retval;
}

static int imtp_connection_read_msg_impl(imtp_connection_t* icon, struct msghdr* msg) {
    int retval = -1;
    int read_len = 0;

    read_len = recvmsg(icon->fd, msg, MSG_DONTWAIT);

    when_true(read_len <= 0, exit);

    retval = 0;
exit:
    return retval;
}

static int imtp_connection_write_impl(imtp_connection_t* icon, void* bmsg, uint32_t len) {
    int retval = -1;

    if(icon->flags & SOCK_DGRAM) {
        SAH_TRACEZ_INFO(ME, "Sending TLV chain over datagram socket");
        retval = sendto(icon->fd, bmsg, len, MSG_DONTWAIT,
                        (struct sockaddr*) icon->addr_other, sizeof(struct sockaddr_un));
    } else if(icon->flags & SOCK_STREAM) {
        SAH_TRACEZ_INFO(ME, "Sending TLV chain over stream socket");
        retval = write(icon->fd, bmsg, len);
    } else {
        retval = -1;
    }
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending data over unix domain socket");
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

static int imtp_connection_write_msg_impl(imtp_connection_t* icon, struct msghdr* msg) {
    int retval = -1;

    if(icon->flags & SOCK_DGRAM || icon->flags & SOCK_STREAM) {
        SAH_TRACEZ_INFO(ME, "Sending message over socket");
        retval = sendmsg(icon->fd, msg, MSG_DONTWAIT);
    } else {
        retval = -1;
    }
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error sending message over unix domain socket");
        goto exit;
    }

    retval = 0;
exit:
    return retval;
}

int imtp_uri_parse(const char* uri,
                   char** scheme,
                   char** host,
                   char** port,
                   char** path) {
    int retval = -1;
    UriUriA parsed_uri;
    amxc_string_t uri_part;
    amxc_string_init(&uri_part, 0);

    when_failed(uriParseSingleUriA(&parsed_uri, uri, NULL), exit);
    if(scheme != NULL) {
        imtp_uri_part_to_string(&uri_part, &parsed_uri.scheme);
        *scheme = amxc_string_take_buffer(&uri_part);
    }
    if(host != NULL) {
        imtp_uri_part_to_string(&uri_part, &parsed_uri.hostText);
        *host = amxc_string_take_buffer(&uri_part);
    }
    if(port != NULL) {
        imtp_uri_part_to_string(&uri_part, &parsed_uri.portText);
        *port = amxc_string_take_buffer(&uri_part);
    }
    if(path != NULL) {
        imtp_uri_path_to_string(&uri_part, parsed_uri.pathHead);
        *path = amxc_string_take_buffer(&uri_part);
    }

    retval = 0;
    uriFreeUriMembersA(&parsed_uri);
exit:
    amxc_string_clean(&uri_part);
    return retval;
}

int imtp_connection_listen(imtp_connection_t** icon, const char* uri, imtp_connection_accept_cb_t fn) {
    int retval = -1;
    char* path = NULL;

    when_null(icon, exit);
    when_null(uri, exit);

    imtp_uri_parse(uri, NULL, NULL, NULL, &path);
    when_null(path, exit);

    *icon = (imtp_connection_t*) calloc(1, sizeof(imtp_connection_t));
    when_null(*icon, exit);

    // Create non-blocking listening socket of type SOCK_STREAM and set flags accordingly
    (*icon)->fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
    when_true((*icon)->fd < 0, exit);
    (*icon)->flags = SOCK_STREAM | IMTP_LISTEN;

    // Set address information based on input uri and bind the file descriptor to an address
    (*icon)->addr_self = (struct sockaddr_un*) calloc(1, sizeof(struct sockaddr_un));
    when_null((*icon)->addr_self, exit);
    (*icon)->addr_self->sun_family = AF_UNIX;
    strncpy((*icon)->addr_self->sun_path, path, sizeof((*icon)->addr_self->sun_path) - 1);

    retval = bind((*icon)->fd, (struct sockaddr*) (*icon)->addr_self, sizeof(struct sockaddr_un));
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error binding stream listen socket on path = %s", path);
    }
    when_failed(retval, exit);

    // Listen for incoming connections
    retval = listen((*icon)->fd, MAX_CONNECTIONS);
    when_failed(retval, exit);

    (*icon)->imtp_connection_accept_cb = fn;

    retval = 0;
exit:
    free(path);
    if(retval < 0) {
        imtp_connection_delete(icon);
    }
    return retval;
}

int imtp_connection_connect(imtp_connection_t** icon, const char* from_uri, const char* to_uri) {
    int retval = -1;
    char* to_path = NULL;

    when_null(icon, exit);

    *icon = (imtp_connection_t*) calloc(1, sizeof(imtp_connection_t));
    when_null(*icon, exit);
    (*icon)->addr_other = (struct sockaddr_un*) calloc(1, sizeof(struct sockaddr_un));
    when_null((*icon)->addr_other, exit);

    // If from_uri is specified, we are creating a datagram socket and binding the socket fd to an
    // address.
    if((from_uri != NULL) && (strlen(from_uri) != 0)) {
        retval = imtp_connection_client_dgram_socket(*icon, from_uri);
    } else { // Create a stream socket
        retval = imtp_connection_client_stream_socket(*icon);
    }
    if(retval < 0) {
        SAH_TRACEZ_ERROR(ME, "Error setting up client socket");
    }

    // If to_uri is specified, set the other party's address and connect to it
    if((to_uri != NULL) && (strlen(to_uri) != 0)) {
        imtp_uri_parse(to_uri, NULL, NULL, NULL, &to_path);
        when_null(to_path, exit);

        (*icon)->addr_other->sun_family = AF_UNIX;
        strncpy((*icon)->addr_other->sun_path, to_path, sizeof((*icon)->addr_other->sun_path) - 1);

        SAH_TRACEZ_INFO(ME, "Connecting to socket on path = %s", to_path);
        retval = connect((*icon)->fd, (struct sockaddr*) (*icon)->addr_other, sizeof(struct sockaddr_un));
        when_failed(retval, exit);
    }

exit:
    free(to_path);
    if(retval < 0) {
        imtp_connection_delete(icon);
    }
    return retval;
}

void imtp_connection_delete(imtp_connection_t** icon) {
    when_null(icon, exit);
    if(*icon != NULL) {
        // Remove connection from list if needed
        amxc_llist_it_take(&(*icon)->it);
        if((*icon)->addr_self != NULL) {
            unlink((*icon)->addr_self->sun_path);
        }
        free((*icon)->addr_self);
        free((*icon)->addr_other);
        close((*icon)->fd);
        free((*icon)->buffer);
    }
    free(*icon);
    *icon = NULL;
exit:
    return;
}

int imtp_connection_accept(imtp_connection_t* icon) {
    int retval = -1;
    int fd_new = -1;
    imtp_connection_t* new_con = NULL;

    when_null(icon, exit);
    when_true(!(icon->flags & IMTP_LISTEN), exit);

    retval = imtp_connection_empty(&new_con);
    when_failed(retval, exit);

    fd_new = accept(icon->fd, NULL, NULL);
    when_true(fd_new < 0, exit);
    new_con->fd = fd_new;
    new_con->flags = SOCK_STREAM;

    if(icon->imtp_connection_accept_cb != NULL) {
        bool acc = icon->imtp_connection_accept_cb(new_con);
        if(!acc) {
            SAH_TRACEZ_WARNING(ME, "Rejecting incoming connection");
            imtp_connection_delete(&new_con);
            goto exit;
        }
    }

    amxc_llist_append(&con_list, &new_con->it);

exit:
    if(fd_new < 0) {
        imtp_connection_delete(&new_con);
    }
    return fd_new;
}

int imtp_connection_peek(imtp_connection_t* icon, void* buffer, uint32_t length) {
    int retval = -1;

    when_null(icon, exit);
    when_null(buffer, exit);

    retval = recv(icon->fd, buffer, length, MSG_DONTWAIT | MSG_PEEK);
    when_true(retval < 0, exit);

    retval = 0;
exit:
    return retval;
}

int imtp_connection_read(imtp_connection_t* icon, imtp_tlv_t** itlv) {
    int retval = -1;
    imtp_tlv_t* tlv_parsed = NULL;

    when_null(icon, exit);
    when_null(itlv, exit);

    // Prefix for an original TLV is just the type of 1 byte
    retval = imtp_connection_read_impl(icon, 1);
    when_failed(retval, exit);

    retval = imtp_message_parse(&tlv_parsed, icon->buffer);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error translating binary message to tlv");
        goto exit;
    }

    (*itlv) = tlv_parsed;
    retval = 0;
exit:
    if((retval != 1) && (icon != NULL)) { // If read finished, reset icon buffer (freeing memory will be done with imtp_tlv_delete)
        icon->read_len = 0;
        icon->buffer = NULL;
    }
    return retval;
}

int imtp_connection_read_frame(imtp_connection_t* icon,
                               imtp_frame_t** frame) {
    int retval = -1;
    int prefix_size = strlen(IMTP_SYNC_USP);

    when_null(icon, exit);
    when_null(frame, exit);

    retval = imtp_connection_read_impl(icon, prefix_size);
    when_failed(retval, exit);

    retval = imtp_frame_parse(frame, icon->buffer);
    if(retval != 0) {
        SAH_TRACEZ_ERROR(ME, "Error translating binary message to tlv");
        goto exit;
    }

    retval = 0;
exit:
    if((retval != 1) && (icon != NULL)) { // If read finished, reset icon buffer (freeing memory will be done with imtp_tlv_delete)
        icon->read_len = 0;
        icon->buffer = NULL;
    }
    return retval;
}

int imtp_connection_read_fd(imtp_connection_t* icon,
                            int* fd) {
    int retval = -1;
    struct msghdr msg = {0};
    struct cmsghdr* cmsg = NULL;
    char buf[CMSG_SPACE(sizeof(int))];
    char data_buf[2];
    struct iovec io = { .iov_base = data_buf, .iov_len = 2 };
    int received_fd;

    when_null(icon, exit);
    when_null(fd, exit);

    memset(buf, '\0', sizeof(buf));

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;
    msg.msg_control = buf;
    msg.msg_controllen = sizeof(buf);

    when_failed(imtp_connection_read_msg_impl(icon, &msg), exit);

    cmsg = CMSG_FIRSTHDR(&msg);
    when_null(cmsg, exit);

    memcpy(&received_fd, CMSG_DATA(cmsg), sizeof(int));

    *fd = received_fd;

    retval = 0;
exit:
    return retval;
}

int imtp_connection_write(imtp_connection_t* icon, imtp_tlv_t* itlv) {
    int retval = -1;
    void* bmsg = NULL;
    uint32_t tlv_header_len = sizeof(uint8_t) + sizeof(uint32_t);

    when_null(icon, exit);
    when_null(itlv, exit);

    retval = imtp_message_to_bytes(itlv, &bmsg);
    when_failed(retval, exit);

    retval = imtp_connection_write_impl(icon, bmsg, itlv->length + tlv_header_len);

exit:
    free(bmsg);
    return retval;
}

int imtp_connection_write_frame(imtp_connection_t* icon,
                                imtp_frame_t* frame) {
    int retval = -1;
    void* bmsg = NULL;
    uint32_t frame_header_len = strlen(IMTP_SYNC_USP) + sizeof(uint32_t);

    when_null(icon, exit);
    when_null(frame, exit);

    retval = imtp_frame_to_bytes(frame, &bmsg);
    when_failed(retval, exit);

    retval = imtp_connection_write_impl(icon, bmsg, frame->length + frame_header_len);

exit:
    free(bmsg);
    return retval;
}

int imtp_connection_write_fd(imtp_connection_t* icon, int fd) {
    int retval = -1;
    struct msghdr msg = {0};
    struct cmsghdr* cmsg;
    char buf[CMSG_SPACE(sizeof(fd))];
    struct iovec io = { .iov_base = (void*) "FD", .iov_len = 2 };

    when_null(icon, exit);

    memset(buf, '\0', sizeof(buf));

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;
    msg.msg_control = buf;
    msg.msg_controllen = sizeof(buf);

    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(fd));
    memcpy(CMSG_DATA(cmsg), &fd, sizeof(fd));

    retval = imtp_connection_write_msg_impl(icon, &msg);

exit:
    return retval;
}

imtp_connection_t* imtp_connection_get_con(int fd) {
    imtp_connection_t* con = NULL;

    amxc_llist_for_each(it, &con_list) {
        con = amxc_llist_it_get_data(it, imtp_connection_t, it);
        if(fd == con->fd) {
            return con;
        }
    }
    return NULL;
}

int imtp_connection_get_fd(imtp_connection_t* icon) {
    int fd = -1;

    if(icon == NULL) {
        goto exit;
    }
    fd = icon->fd;
exit:
    return fd;
}

int imtp_connection_get_flags(const imtp_connection_t* const icon) {
    int retval = -1;

    when_null(icon, exit);

    retval = icon->flags;

exit:
    return retval;
}
