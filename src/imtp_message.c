/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

#include <debug/sahtrace.h>

#include <amxc/amxc_macros.h>

#include "imtp/imtp_message.h"
#include "imtp_priv.h"

/* Serialize all data in the passed tlv and write it to the address specified by bytes */
void imtp_tlv_serialize(imtp_tlv_t* itlv, uint8_t* bytes) {
    uint8_t* ptr = bytes;
    uint32_t len_nbo = 0;

    memcpy(ptr, &(itlv->type), sizeof(uint8_t));
    ptr += sizeof(uint8_t);

    len_nbo = htonl(itlv->length);
    memcpy(ptr, &len_nbo, sizeof(uint32_t));
    ptr += sizeof(uint32_t);
    memcpy(ptr, itlv->value, itlv->length);

    return;
}

int imtp_tlv_new(imtp_tlv_t** itlv,
                 imtp_tlv_type_t type,
                 uint32_t len,
                 void* value,
                 uint32_t offset,
                 uint32_t flags) {
    int retval = -1;
    imtp_tlv_t* tlv = NULL;

    when_null(itlv, exit);
    when_true(type >= imtp_tlv_type_max, exit);
    when_true(value == NULL && flags == IMTP_TLV_COPY, exit);

    tlv = (imtp_tlv_t*) calloc(1, sizeof(imtp_tlv_t));
    when_null(tlv, exit);

    tlv->type = type;
    tlv->length = len;
    if(flags == IMTP_TLV_TAKE) {
        tlv->value = value;
        tlv->offset = offset;
    } else if(flags == IMTP_TLV_COPY) {
        tlv->value = calloc(1, len);
        memcpy(tlv->value, ((char*) value) + offset, len);
        tlv->offset = 0;
    } else {
        free(tlv);
        goto exit;
    }

    *itlv = tlv;
    retval = 0;
exit:
    return retval;
}

void imtp_tlv_delete(imtp_tlv_t** itlv) {
    imtp_tlv_t* tlv_next = NULL;

    when_null(itlv, exit);
    when_null(*itlv, exit);

    tlv_next = (*itlv)->next;
    free((*itlv)->value);
    free(*itlv);
    while(tlv_next != NULL) {
        imtp_tlv_t* tlv_temp = tlv_next->next;

        if(tlv_next->offset == 0) {
            free(tlv_next->value);
        }
        free(tlv_next);
        tlv_next = tlv_temp;
    }
    *itlv = NULL;

exit:
    return;
}

int imtp_tlv_add(imtp_tlv_t* tlv_head, imtp_tlv_t* tlv_new) {
    int retval = -1;
    imtp_tlv_t* tlv_last = tlv_head;

    when_null(tlv_head, exit);
    when_null(tlv_new, exit);

    while(tlv_last->next != NULL) {
        tlv_last = tlv_last->next;
    }
    tlv_last->next = tlv_new;

    tlv_head->length += tlv_new->length + sizeof(uint8_t) + sizeof(uint32_t);

    retval = 0;
exit:
    return retval;
}

const imtp_tlv_t* imtp_message_get_next_tlv(const imtp_tlv_t* itlv, imtp_tlv_type_t type) {
    const imtp_tlv_t* tlv_res = NULL;
    imtp_tlv_t* tlv_temp = NULL;

    when_null(itlv, exit);
    when_true(itlv->type >= imtp_tlv_type_max, exit);

    tlv_temp = itlv->next;
    while(tlv_temp != NULL) {
        if(tlv_temp->type == type) {
            tlv_res = tlv_temp;
            break;
        }
        tlv_temp = tlv_temp->next;
    }

exit:
    return tlv_res;
}

const imtp_tlv_t* imtp_message_get_first_tlv(const imtp_tlv_t* tlv_head, imtp_tlv_type_t type) {
    const imtp_tlv_t* tlv_res = NULL;

    when_null(tlv_head, exit);

    tlv_res = imtp_message_get_next_tlv(tlv_head, type);

exit:
    return tlv_res;
}

int imtp_message_to_bytes(imtp_tlv_t* itlv, void** bmsg) {
    int retval = -1;
    imtp_tlv_t* tlv_next = NULL;
    uint8_t* ptr = NULL;
    uint32_t len_nbo = 0;

    when_null(itlv, exit);
    when_null(bmsg, exit);
    when_true(itlv->type != imtp_tlv_type_head, exit);

    *bmsg = calloc(1, itlv->length + sizeof(uint8_t) + sizeof(uint32_t));
    when_null(*bmsg, exit);

    ptr = (uint8_t*) (*bmsg);

    memcpy(ptr, &(itlv->type), sizeof(uint8_t));
    ptr += sizeof(uint8_t);
    len_nbo = htonl(itlv->length);
    memcpy(ptr, &len_nbo, sizeof(uint32_t));
    ptr += sizeof(uint32_t);

    tlv_next = itlv->next;
    while(tlv_next != NULL) {
        imtp_tlv_serialize(tlv_next, ptr);
        ptr += tlv_next->length + sizeof(uint8_t) + sizeof(uint32_t);
        tlv_next = tlv_next->next;
    }

    retval = 0;
exit:
    return retval;
}

imtp_tlv_t* imtp_message_read_next_tlv(uint8_t* bmsg_ptr, uint32_t offset, void* bmsg) {
    imtp_tlv_t* tlv_next = NULL;
    imtp_tlv_type_t tlv_type = 0;
    uint32_t tlv_len = 0;
    uint32_t len_nbo = 0;

    memcpy(&tlv_type, bmsg_ptr, sizeof(uint8_t));
    bmsg_ptr += sizeof(uint8_t);
    memcpy(&len_nbo, bmsg_ptr, sizeof(uint32_t));
    tlv_len = ntohl(len_nbo);

    if(imtp_tlv_new(&tlv_next, tlv_type, tlv_len, bmsg,
                    offset + sizeof(uint8_t) + sizeof(uint32_t), IMTP_TLV_TAKE) < 0) {
        SAH_TRACEZ_ERROR(ME, "Error creating new tlv with type = [%d], length = [%d]", tlv_type, tlv_len);
        goto exit;
    }

exit:
    return tlv_next;
}

int imtp_message_parse(imtp_tlv_t** itlv, void* bmsg) {
    int retval = -1;
    uint8_t* ptr = NULL;
    uint32_t processed_len = 0;
    uint32_t total_len = 0;
    uint32_t len_nbo = 0;

    when_null(itlv, exit);
    when_null(bmsg, exit);

    retval = imtp_tlv_new(itlv, imtp_tlv_type_head, 0, bmsg, 0, IMTP_TLV_TAKE);
    when_failed(retval, exit);

    ptr = (uint8_t*) bmsg;
    memcpy(&(*itlv)->type, ptr, sizeof(uint8_t));
    ptr += sizeof(uint8_t);
    memcpy(&len_nbo, ptr, sizeof(uint32_t));
    total_len = ntohl(len_nbo);
    ptr += sizeof(uint32_t);

    processed_len = sizeof(uint8_t) + sizeof(uint32_t);
    // Read out all incoming TLVs
    while(processed_len < total_len) {
        imtp_tlv_t* tlv_next = imtp_message_read_next_tlv(ptr, processed_len, bmsg);
        if(imtp_tlv_add(*itlv, tlv_next) < 0) {
            imtp_tlv_delete(&tlv_next);
            goto exit;
        }
        ptr += tlv_next->length + sizeof(uint8_t) + sizeof(uint32_t);
        processed_len += tlv_next->length + sizeof(uint8_t) + sizeof(uint32_t);
    }

    retval = 0;
exit:
    return retval;
}
