/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IMTP_MESSAGE_H__)
#define __IMTP_MESSAGE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#define IMTP_TLV_TAKE (0x0)
#define IMTP_TLV_COPY (0x1)

/**
   @file
   @brief
   internal message protocol header file
 */

/**
   @ingroup imtp
   @defgroup imtp_message imtp message
 */

/**
   @ingroup imtp_message
   @brief
   Each TLV field has a certain type, indicated by an integer.
   The types that can be specified are indicated here.
   Types 200 and higher are custom TLV types. Anything below 200 is reserved for standardized TLV
   types.
 */
typedef enum _imtp_tlv_type {
    imtp_tlv_type_head,             // payload is NULL
    imtp_tlv_type_handshake,        // payload is a UTF-8 string that represents the Endpoint ID of the USP Endpoint
    imtp_tlv_type_error,            // payload is a UTF-8 string that provides the error message
    imtp_tlv_type_protobuf_bytes,   // payload is binary protobuf
    imtp_tlv_type_topic = 200,      // payload is a string with an MQTT topic
    imtp_tlv_type_subscribe,        // payload is an object path string
    imtp_tlv_type_unsubscribe,      // payload is an object path string
    imtp_tlv_type_json,             // payload is a json string
    imtp_tlv_type_mqtt_props,       // payload are json encoded MQTT properties
    imtp_tlv_type_packet_id,        // payload is a network packet identifier
    imtp_tlv_type_packet_meta,      // payload is a network packet meta data
    imtp_tlv_type_packet_payload,   // payload is a network packet payload
    imtp_tlv_type_packet_verdict,   // payload is a network packet verdict
    imtp_tlv_type_pattern,          // payload is a string with an MQTT pattern (SAH specific)
    imtp_tlv_type_fd,               // type indicating a file descriptor will be sent over the socket
    imtp_tlv_type_eid = 250,        // (DEPRECATED) payload is a string with the Endpoint ID
    imtp_tlv_type_max
} imtp_tlv_type_t;

typedef struct _imtp_tlv {
    imtp_tlv_type_t type;   /* defines the tlv type */
    uint32_t length;        /* length of the tlv value */
    void* value;            /* byte array with payload */
    uint32_t offset;        /* offset from value address where data starts */
    struct _imtp_tlv* next; /* pointer to the next imtp_tlv_t */
} imtp_tlv_t;

/**
   @ingroup imtp_message
   @brief
   Creates an empty imtp tlv structure.

   @note
   A new imtp_tlv_t structure is created on the heap. Its values are correctly initialized.
   The flags argument determines whether ownership of the value should be taken by the TLV or
   if the data should be copied. When flags == IMTP_TLV_COPY only the part of the passed buffer
   pointed to by 'value' is copied, starting from (value + offset) and 'len' bytes are copied.
   The resulting TLV will have an offset of 0.
   When flags == IMTP_TLV_TAKE it is assumed that the data is already allocated on the heap.
   Use @ref imtp_tlv_delete to free the memory

   @param itlv a pointer to a pointer to the newly created imtp_tlv_t.
   @param type a valid TLV type.
   @param len the length of the TLV data.
   @param value a pointer to the TLV value. This can be NULL if the type is imtp_tlv_type_head.
   @param offset an offset from the pointer value in case multiple TLVs are defined in a contiguous block.
   @param flags can be IMTP_TLV_TAKE to take ownership or IMTP_TLV_COPY to copy the data.

   @return 0 if the tlv is correctly created, -1 if the creation went wrong.
 */
int imtp_tlv_new(imtp_tlv_t** itlv,
                 imtp_tlv_type_t type,
                 uint32_t len,
                 void* value,
                 uint32_t offset,
                 uint32_t flags);

/**
   @ingroup imtp_message
   @brief
   Free the memory for a single TLV.

   @note
   Destroy(free) the imtp_tlv_t structure and set the resulting pointer to NULL. Also frees all
   TLVs that are pointed to by the next pointers.
   The memory of the TLV 'value' will only be freed if the TLV has offset 0. If its offset is
   different from zero, it is assumed that there exists another TLV in the chain that does have
   an offset of 0. The memory for all TLV values with the same 'value' address will be freed at
   the same time by calling free() once for the TLV with offset 0.

   @param itlv must be a pointer to a pointer to an imtp_tlv_t structure.

 */
void imtp_tlv_delete(imtp_tlv_t** itlv);

/**
   @ingroup imtp_message
   @brief
   Add an extra imtp_tlv_t to the imtp message.

   @note
   It is possible to add a number of TLVs to the imtp_tlv_t structure. Each TLV points to the next
   TLV until the last one is reached. It is possible to have more than one TLV with the same type.
   TLVs are assumed to be created with @ref imtp_tlv_new. Freeing memory for the TLVs can be
   done by calling @ref imtp_tlv_delete on the tlv head.

   @param tlv_head a pointer to a imtp_tlv_t with type 0.
   @param tlv_new a pointer to a TLV which will be added to the TLV chain

   @return return 0 if the TLV was successfully added,
   -1 if the TLV could not be added and an error occured.
 */
int imtp_tlv_add(imtp_tlv_t* tlv_head, imtp_tlv_t* tlv_new);

/**
   @ingroup imtp_message
   @brief
   Return the next tlv instance of a certain type in the imtp message.

   @note
   An imtp message can contain a number of TLVs. Each TLV points to the next TLV until the last one
   is reached. This function returns the next TLV of a type given a previous instance of that type.

   @param itlv must be a pointer to a valid imtp_tlv_t structure.
   @param type is requested type

   @return a pointer to the next imtp_header_t (TLV) given a type
   return NULL if no such header is found or in case of error.
 */
const imtp_tlv_t* imtp_message_get_next_tlv(const imtp_tlv_t* itlv, imtp_tlv_type_t type);

/**
   @ingroup imtp_message
   @brief
   Return the first header instance of a certain type in the imtp message.

   @note
   An imtp message can contain a number of TLVs. Each TLV points to the next TLV until the last one
   is reached. This function returns the first instance of a given type starting from the provided
   TLV.

   @param tlv_head must be a pointer to a valid imtp_tlv_t structure
   @param type is requested type

   @return a pointer to the first imtp_tlv_t given a type
   return NULL if no such TLV is found or in case of error.
 */
const imtp_tlv_t* imtp_message_get_first_tlv(const imtp_tlv_t* tlv_head, imtp_tlv_type_t type);

/**
   @ingroup imtp_message
   @brief
   Convert imtp_tlv_t chain to a byte array.

   @note
   An imtp message exists out of a chain of multiple TLVs. These TLVs will be serialized to a byte
   array to allow the message to be transmitted. Data will be allocated for the byte array. It
   must be freed when it is no longer needed.

   @param itlv a pointer to a valid imtp_tlv_t with type 0.
   @param bmsg a pointer to a pointer for the byte array.

   @return 0 if the byte array was correctly created, -1 in case of error.
 */
int imtp_message_to_bytes(imtp_tlv_t* itlv, void** bmsg);

/**
   @ingroup imtp_message
   @brief
   Parse a byte array of a given length into multiple imtp_tlv_t.

   @note
   Try to parse the byte array in a valid imtp_tlv_t format. The byte array can contain multiple
   TLV messages. Each of these messages will be appended to the chain. Instead of allocating new
   memory for the value of each TLV, the same pointer to the start of the byte array is given as
   value for the TLVs. The offset parameter is used to get to the individual TLV values.

   @param itlv must be a pointer to a pointer for a new imtp_tlv_t structure.
   @param bmsg is a pointer to a byte array

   @return If the parsing is successful, return 0,
   if an error occured return -1
 */
int imtp_message_parse(imtp_tlv_t** itlv, void* bmsg);

#ifdef __cplusplus
}
#endif

#endif // __IMTP_MESSAGE_H__
