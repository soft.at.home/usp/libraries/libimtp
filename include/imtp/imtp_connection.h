/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IMTP_CONNECTION_H__)
#define __IMTP_CONNECTION_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "imtp/imtp_message.h"
#include "imtp/imtp_frame.h"
#include "imtp/imtp_common.h"

#define IMTP_LISTEN (0x0100) /* Indicates whether the socket is a listen socket */

/**
   @file
   @brief
   USP communication header file.
 */
/**
   @ingroup imtp
   @defgroup imtp_connection imtp connection
 */

/**
   @ingroup imtp_connection
   @brief
   Each endpoint can have an addr_self which contains its own uri and an addr_other which contains
   the uri of its correspondent. The values of addr_self are set on creation of a new
   imtp_connection_t by using @ref imtp_connection_connect when using datagram sockets or
   @ref imtp_connection_listen when using stream sockets.

   The values of addr_other are set either by calling @ref imtp_connection_connect as a client
   (for both datagram and stream sockets) or by calling @ref imtp_connection_read when using
   datagram sockets to allow an endpoint to reply to its correspondent.

   A connection accept callback function can be passed when calling @ref imtp_connection_listen.
   This callback can be used to check if an incoming connection should be accepted or dropped.

   The flags field is used internally to determine whether we are dealing with a datagram,
   stream and/or listen socket.

   When reading incoming data with @ref imtp_connection_read, this data will initially be stored
   in an internal buffer. It is kept there until the full IMTP message is read. The read_len field
   is used to track how many bytes of the current message have already been read.
 */
typedef struct _imtp_connection imtp_connection_t;

/**
   @ingroup imtp_connection
   @brief
   Callback function definition for accepting an incoming connection on a listening socket.
 */
typedef bool (* imtp_connection_accept_cb_t)(imtp_connection_t*);

/**
   @ingroup imtp_connection
   @brief
   Create a new listening socket.

   @note
   This function creates an imtp_connection_t structure. Use @ref imtp_connection_delete
   to free the memory.

   @param icon pointer to a pointer to the newly created imtp_connection_t structure.
   @param uri a uri name (rfc 3986), based on this input, the correct socket interface is being created.
   @param fn pointer to a callback function that will be called when there is data on icon->fd

   @return 0 if the imtp_connection is succesfully created, -1 if an error occured.
 */
int imtp_connection_listen(imtp_connection_t** icon, const char* uri, imtp_connection_accept_cb_t fn);

/**
   @ingroup imtp_connection
   @brief
   Connect to the socket specified by the uri.

   @note
   This call makes a connection with the remote site

   @param icon pointer to a pointer for a new imtp_connection_t structure.
   @param from_uri (can be NULL) a uri name (rfc 3986), based on this input, a connection is made to another endpoint
   @param to_uri (can be NULL) a uri name (rfc 3986), based on this input, a connection is made to another endpoint

   @return 0 if success, -1 if an error occured
 */
int imtp_connection_connect(imtp_connection_t** icon, const char* from_uri, const char* to_uri);

/**
   @ingroup imtp_connection
   @brief
   Destroys and frees the memory of the imtp_connection_t.

   @note
   This function destroys an imtp_connection_t structure.
   The assigned memory is freed and the pointer is set to NULL.

   @param icon a pointer to a pointer to a valid imtp_connection_t structure.
 */
void imtp_connection_delete(imtp_connection_t** icon);

/**
   @ingroup imtp_connection
   @brief
   Accept a new connection on an listen socket.

   @param icon a pointer to a valid imtp_connection_t structure.

   @return file descriptor for the new connection, -1 if an error occured
 */
int imtp_connection_accept(imtp_connection_t* icon);

/**
   @ingroup imtp_connection
   @brief
   Receives an IMTP TLV message over an IMTP connection.

   @note
   A message is received over the IMTP connection and parsed into an imtp_tlv_t structure.
   The imtp_tlv_t is created on the heap using @ref imtp_tlv_new and needs to be destroyed
   using @ref imtp_tlv_delete.

   @note
   It is possible that the entire IMTP message is too long to be read at once. If this happens,
   the function will return with value 1 and store the data that was already read internally in the
   passed imtp_connection_t struct. The function must be called again when more data is ready to be
   received. When the function returns with value 0, the message was correctly received and can be
   found in the imtp_tlv_t struct.

   @warning
   This function was added before the IMTP was standardized. It is recommended to use
   @ref imtp_connection_read_frame with standardized IMTP frames instead.

   @param icon pointer to a imtp_connection_t.
   @param itlv pointer to a pointer to an imtp_tlv_t struct that will be created.

   @return 0 if the message is successfully received from the connection,
           -1 if an error occured,
           1 if the message could not be read entirely with a single read
 */
int imtp_connection_read(imtp_connection_t* icon, imtp_tlv_t** itlv);

/**
   @ingroup imtp_connection
   @brief
   Receives an IMTP frame over an IMTP connection.

   @note
   A message is received over the IMTP connection and parsed into a imtp_frame_t structure.
   The imtp_frame_t is created on the heap using @ref imtp_frame_new and needs to be destroyed
   using @ref imtp_frame_delete. Any TLVs that are part of the IMTP message will also be destroyed.

   @note
   It is possible that the entire IMTP message is too long to be read at once. If this happens,
   the function will return with value 1 and store the data that was already read internally in the
   passed imtp_connection_t struct. The function must be called again when more data is ready to be
   received. When the function returns with value 0, the message was correctly received and can be
   found in the imtp_frame_t struct.

   @param icon pointer to a imtp_connection_t.
   @param frame pointer to a pointer to an imtp_frame_t struct that will be created.

   @return 0 if the message is successfully received from the connection,
           -1 if an error occured,
           1 if the message could not be read entirely with a single read
 */
int imtp_connection_read_frame(imtp_connection_t* icon,
                               imtp_frame_t** frame);

/**
   @ingroup imtp_connection
   @brief
   Receives a file descriptor over an IMTP connection.

   @note
   A file descriptor is received over the IMTP connection if one was sent.

   @param icon pointer to a imtp_connection_t.
   @param fd pointer to the file descriptor that will be received.

   @return 0 if the file descriptor is successfully received from the connection,
           -1 if an error occured
 */
int imtp_connection_read_fd(imtp_connection_t* icon,
                            int* fd);

/**
   @ingroup imtp_connection
   @brief
   Sends a TLV message over an IMTP connection.

   @note
   Given a valid imtp_connection and a valid imtp_tlv_t, the message is sent over the configured
   connection.

   @warning
   This function was added before the IMTP was standardized. It is recommended to use
   @ref imtp_connection_write_frame with standardized IMTP frames instead.

   @param icon pointer to a valid imtp_connection_t structure.
   @param itlv pointer to a valid imtp_tlv_t structure.

   @return 0 if the message is successfully sent over the connection, if an error occured -1 is returned.
 */
int imtp_connection_write(imtp_connection_t* icon, imtp_tlv_t* itlv);

/**
   @ingroup imtp_connection
   @brief
   Sends a IMTP frame over an IMTP connection.

   @note
   Given a valid IMTP connection and a valid IMTP frame, the message is sent over the configured
   connection.

   @param icon pointer to a valid imtp_connection_t structure.
   @param frame pointer to a valid imtp_frame_t structure.

   @return 0 if the message is successfully sent over the connection, -1 otherwise.
 */
int imtp_connection_write_frame(imtp_connection_t* icon,
                                imtp_frame_t* frame);

/**
   @ingroup imtp_connection
   @brief
   Sends a file descriptor over an IMTP connection.

   @note
   Given a valid IMTP connection and a valid file descriptor, the file descriptor is sent over the configured
   connection.

   @param icon pointer to a valid imtp_connection_t structure.
   @param fd file descriptor to send.

   @return 0 if the file descriptor is successfully sent over the connection, -1 otherwise.
 */
int imtp_connection_write_fd(imtp_connection_t* icon, int fd);

/**
   @ingroup imtp_connection
   @brief
   Read data from the IMTP connection without removing it from the queue. Subsequent calls will
   return the same data.

   @note
   Read out the first 'length' bytes of the imtp message. Can be used to verify the type and length
   of the incoming message and allocate memory for the rest of the message. These 'length' bytes are
   not removed from the queue, but can be read again by another read/recv call.

   @param icon pointer to a valid imtp_connection_t structure.
   @param buffer a buffer of length bytes.
   @param length the amount of bytes to read.

   @return 0 if the desired amount of bytes are successfully read, -1 if an error occured. In case
           of failure errno is set to the value described in the man page of recv.
 */
int imtp_connection_peek(imtp_connection_t* icon, void* buffer, uint32_t length);

/**
   @ingroup imtp_connection
   @brief
   Try to get an imtp_connection_t struct based on a file descriptor.

   @param fd an open file descriptor

   @return A pointer to the desired imtp_connection_t if found, NULL in case no result is found.
 */
imtp_connection_t* imtp_connection_get_con(int fd);

/**
   @ingroup imtp_connection
   @brief
   Get file descriptor from struct.

   @param icon a pointer to a valid imtp_connection_t struct

   @return File descriptor that corresponds to the connection, -1 in case of error.
 */
int imtp_connection_get_fd(imtp_connection_t* icon);

/**
   @ingroup imtp_connection
   @brief
   Get the IMTP connection flags.

   @param icon a pointer to a valid imtp_connection_t struct

   @return The flags associated with the connection, -1 if a NULL pointer is provided.
 */
int imtp_connection_get_flags(const imtp_connection_t* const icon);

/**
   @ingroup imtp_connection
   @brief
   Parses a uri into a scheme, host, port and path.

   @param uri a pointer to the uri to parse.
   @param scheme a pointer to a pointer of the scheme.
   @param host a pointer to a pointer of the host.
   @param port a pointer to a pointer of the port.
   @param path a pointer to a pointer to the output path.

   @return 0 if the operation was successful and -1 if an error occurred.
 */
int imtp_uri_parse(const char* uri, char** scheme, char** host, char** port, char** path);

#ifdef __cplusplus
}
#endif

#endif // __IMTP_CONNECTION_H__
