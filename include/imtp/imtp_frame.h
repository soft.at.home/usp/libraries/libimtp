/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__IMTP_FRAME_H__)
#define __IMTP_FRAME_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdint.h>

#include "imtp_message.h"

#define IMTP_SYNC_USP "_USP" /* Frame synchronization bytes */

/**
   @file
   @brief
   Internal Message Transfer Protocol frame definitions
 */

/**
   @ingroup imtp
   @defgroup imtp_frame imtp frame
 */

typedef struct _imtp_frame {
    uint32_t length;        /* length of the TLV messages that follow */
    struct _imtp_tlv* tlv;  /* pointer to the first imtp_tlv_t */
} imtp_frame_t;

/**
   @ingroup imtp_frame
   @brief
   Creates a new imtp_frame_t struct on the heap

   @note
   Memory is allocated on the heap by this function. Use @ref imtp_frame_delete with the same
   pointer to free the memory.

   @param frame pointer to a pointer for the new frame
   @return 0 in case of success, -1 in case of error
 */
int imtp_frame_new(imtp_frame_t** frame);

/**
   @ingroup imtp_frame
   @brief
   Frees the memory allocated for the frame and sets the pointer of the struct to NULL. Also frees
   the TLVs that are pointed to by the frame.

   @param frame pointer to a pointer of a frame that was previously allocated with @ref imtp_frame_new
 */
void imtp_frame_delete(imtp_frame_t** frame);

/**
   @ingroup imtp_frame
   @brief
   Add an extra imtp_tlv_t to the IMTP frame.

   @note
   It is possible to add a number of TLVs to the IMTP frame. Each TLV points to the next TLV until
   the last one is reached. It is possible to have more than one TLV with the same type.
   TLVs are assumed to be created with @ref imtp_tlv_new. Freeing memory for the TLVs can be
   done by calling @ref imtp_frame_delete on the frame.

   @param frame pointer to the IMTP frame header
   @param tlv_new pointer to a TLV which will be added to the TLV chain
   @return 0 in case the TLV was successfully added, -1 otherwise
 */
int imtp_frame_tlv_add(imtp_frame_t* frame, imtp_tlv_t* tlv_new);

/**
   @ingroup imtp_frame
   @brief
   Convert an IMTP frame to a byte array.

   @note
   An IMTP frame consists out of a header and a one or multiple TLVs. The header of the frame will
   contain 4 fixed synchronization bytes, 0x5f, 0x55, 0x53, 0x50, which spell "_USP".

   After the synchronization bytes, the length of the TLVs will be encoded as a 4 byte unsigned
   integer in network byte order. Then the TLVs will be added to the byte array. The TLV Type will
   be a single byte and the Length will again be a 4 byte unsigned integer in network byte order.

   Data will be allocated for the byte array, which must be freed when it is no longer needed.

   @param frame a pointer to a valid IMTP frame
   @param bmsg a pointer to a pointer for the byte array

   @return 0 if the byte array was correctly created, -1 in case of error
 */
int imtp_frame_to_bytes(imtp_frame_t* frame, void** bmsg);

/**
   @ingroup imtp_frame
   @brief
   Parse a byte array of a given length into an imtp_frame_t with TLVs.

   @note
   Try to parse the byte array in a valid imtp_frame_t format. The byte array will contain a
   synchronization part, of 4 bytes, 0x5f, 0x55, 0x53, 0x50, which spell "_USP". After the sync
   part, the length of the TLV messages will be encoded, followed by the actual TLVs.
   Instead of allocating new memory for the value of each TLV, the same pointer to the start of the
   byte array is given as value for the TLVs. The offset parameter is used to get to the individual
   TLV values.

   @param frame must be a pointer to a pointer for a new imtp_frame_t structure.
   @param bmsg is a pointer to a byte array

   @return 0 if the parsing is successful, -1 in case of error
 */
int imtp_frame_parse(imtp_frame_t** frame, void* bmsg);

/**
   @ingroup imtp_frame
   @brief
   Return the first TLV of a certain type in the imtp frame.

   @note
   An imtp frame can contain a number of TLVs. Each TLV points to the next TLV until the last one
   is reached. This function returns the first instance of a given type starting from the frame

   @note
   When the next TLV of a given type must be found, you can use @ref imtp_message_get_next_tlv

   @param frame must be a pointer to a valid imtp_frame_t structure
   @param type requested TLV type

   @return a pointer to the first imtp_tlv_t given a type
           or NULL if no such TLV is found or in case of error.
 */
const imtp_tlv_t* imtp_frame_get_first_tlv(const imtp_frame_t* frame, imtp_tlv_type_t type);

#ifdef __cplusplus
}
#endif

#endif // __IMTP_FRAME_H__
