SRCDIR = $(realpath ../../src)
DUMMYDIR = $(realpath ../../test/dummy_functions)
SOURCES = $(wildcard $(SRCDIR)/*.c) $(wildcard $(DUMMYDIR)/*.c)

INCDIR = $(realpath ../../include ../../include_priv ../include/imtp ../../test/dummy_functions)
CFLAGS += $(addprefix -I, $(INCDIR))
LDFLAGS += -lamxc -luriparser -lsahtrace
