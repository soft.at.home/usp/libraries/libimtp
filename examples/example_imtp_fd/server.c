/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <amxc/amxc_macros.h>

#include "imtp/imtp_connection.h"

#define SERVER_SOCK_FILE "/tmp/server.sock"
#define TEST_STRING "Hello World\n"

static int catch_signals(int* sfd) {
    sigset_t mask;

    /* Handle SIGINT */
    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);

    /* Set the signals from mask to the SIG_BLOCK set */
    if(sigprocmask(SIG_BLOCK, &mask, NULL) < 0) {
        return -1;
    }

    *sfd = signalfd(-1, &mask, 0);
    if(*sfd < 0) {
        return -1;
    }

    return 0;
}

static int handle_signal(int sfd) {
    int retval = 0;
    struct signalfd_siginfo si;

    retval = read(sfd, &si, sizeof(si));
    if((retval < 0) || (retval != sizeof(si))) {
        perror("read");
        goto exit;
    }
    if(si.ssi_signo == SIGINT) {
        retval = SIGINT;
        printf("Shut down\n");
        goto exit;
    }
exit:
    return retval;
}

static int handle_connection(imtp_connection_t* icon) {
    int fd_new = -1;
    int retval = -1;
    int nr_of_polls = 0;
    imtp_connection_t* con_new = NULL;
    struct pollfd fds[1];
    int fd = -1;

    printf("Handling new incoming connection\n");
    fd_new = imtp_connection_accept(icon);
    when_true(fd_new < 0, exit);

    printf("Getting connection based on fd\n");
    con_new = imtp_connection_get_con(fd_new);
    when_null(con_new, exit);

    printf("Blocking read, because there is no evloop\n");
    while(1) {
        retval = imtp_connection_read_fd(con_new, &fd);
        if(retval == 0) {
            break;
        }
        sleep(1);
    }

    when_true(fd < 0, exit);

    printf("Writing to file");
    write(fd, TEST_STRING, strlen(TEST_STRING));
    close(fd);

    retval = 0;

exit:
    return retval;
}

int main() {
    int retval = 0;
    int nr_of_polls = 0;
    imtp_connection_t* icon = NULL;
    struct pollfd fds[2] = {};
    int sfd = -1; // signal fd

    retval = imtp_connection_listen(&icon, SERVER_SOCK_FILE, NULL);
    when_failed(retval, exit);

    /* Catch interrupt signal */
    retval = catch_signals(&sfd);
    if(retval < 0) {
        printf("Error catch_signals\n");
        goto exit;
    }

    /* Polling */
    fds[0].fd = imtp_connection_get_fd(icon);
    fds[0].events = POLLIN;
    nr_of_polls++;

    fds[1].fd = sfd;
    fds[1].events = POLLIN;
    nr_of_polls++;

    while(1) {
        retval = poll(fds, nr_of_polls, -1);
        if(fds[0].revents & POLLIN) {
            retval = handle_connection(icon);
            if(retval < 0) {
                goto exit;
            }
        }
        if(fds[1].revents & POLLIN) {
            retval = handle_signal(sfd);
            if((retval < 0) || (retval == SIGINT)) {
                goto exit;
            }
        }
    }

exit:
    if(icon != NULL) {
        imtp_connection_delete(&icon);
    }
    if(sfd >= 0) {
        close(sfd);
    }
    return retval;
}