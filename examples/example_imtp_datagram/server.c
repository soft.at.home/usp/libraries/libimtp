/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/signalfd.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "imtp/imtp_message.h"
#include "imtp/imtp_connection.h"
#include "imtp_priv.h"

#define SERVER_SOCK_FILE "/tmp/server.sock"

static int catch_signals(int* sfd) {
    sigset_t mask;

    /* Handle SIGINT */
    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);

    /* Set the signals from mask to the SIG_BLOCK set */
    if(sigprocmask(SIG_BLOCK, &mask, NULL) < 0) {
        return -1;
    }

    *sfd = signalfd(-1, &mask, 0);
    if(*sfd < 0) {
        return -1;
    }

    return 0;
}

static int handle_signal(int sfd) {
    int retval = 0;
    struct signalfd_siginfo si;

    retval = read(sfd, &si, sizeof(si));
    if((retval < 0) || (retval != sizeof(si))) {
        perror("read");
        goto exit;
    }
    if(si.ssi_signo == SIGINT) {
        retval = SIGINT;
        printf("Shut down\n");
        goto exit;
    }
exit:
    return retval;
}

static int handle_message(imtp_connection_t* icon) {
    int retval = -1;
    imtp_tlv_t* tlv = NULL;
    imtp_tlv_t* tlv_next = NULL;

    /* Receive data on socket */
    retval = imtp_connection_read(icon, &tlv);
    if(retval < 0) {
        printf("Error imtp_connection_recv\n");
        goto exit;
    }

    if(tlv->type != imtp_tlv_type_head) {
        printf("TLV does not start with correct type: [%d]\n", tlv->type);
        retval = -1;
        goto exit;
    }

    tlv_next = tlv->next;
    while(tlv_next != NULL) {
        printf("Current tlv has length: %d\n", tlv_next->length);
        if(tlv_next->type == imtp_tlv_type_topic) {
            printf("Topic = %s\n", (char*) tlv_next->value + tlv_next->offset);
        } else {
            printf("TLV type not supported in this example\n");
        }

        tlv_next = tlv_next->next;
    }

    retval = 0;
exit:
    imtp_tlv_delete(&tlv);
    return retval;
}

int main() {
    int retval = 0;
    int nr_of_polls = 0;
    imtp_connection_t* icon = NULL;
    struct pollfd fds[2] = {};
    int sfd = -1; // signal fd

    retval = imtp_connection_connect(&icon, SERVER_SOCK_FILE, NULL);

    /* Catch interrupt signal */
    retval = catch_signals(&sfd);
    if(retval < 0) {
        printf("Error catch_signals\n");
        goto exit;
    }

    /* Polling */
    fds[0].fd = imtp_connection_get_fd(icon);
    fds[0].events = POLLIN;
    nr_of_polls++;

    fds[1].fd = sfd;
    fds[1].events = POLLIN;
    nr_of_polls++;

    while(1) {
        retval = poll(fds, nr_of_polls, -1);
        if(fds[0].revents & POLLIN) {
            retval = handle_message(icon);
            if(retval < 0) {
                goto exit;
            }
        }
        if(fds[1].revents & POLLIN) {
            retval = handle_signal(sfd);
            if((retval < 0) || (retval == SIGINT)) {
                goto exit;
            }
        }
    }

exit:
    if(icon != NULL) {
        imtp_connection_delete(&icon);
    }
    if(sfd >= 0) {
        close(sfd);
    }
    return retval;
}